from django.db import models
from admins.models import StrengthSequence


class StandingOrderInformation(models.Model):
    owner_user_id = models.IntegerField(null=False)
    user_id = models.CharField(max_length=25, null=False)
    rebilling_id = models.CharField(max_length=255, null=False)
    order_date = models.DateTimeField(auto_now=False,null=False)
    last_success_rebilling_date = models.DateTimeField(auto_now=False, null=True)
    rebilling_period = models.IntegerField(default=30)
    next_rebilling = models.DateTimeField(auto_now=False, null=True)
    delivery_period = models.IntegerField(default=56)
    next_delivery_printed = models.DateTimeField(auto_now=False, null=True)
    last_delivery_printed =  models.DateTimeField(auto_now=False, null=True)
    last_delivery_batch_id = models.CharField(max_length=100, null=True)
    unsubscribe_recived = models.DateTimeField(auto_now=False, null=True)
    unsubscribe_approved = models.DateTimeField(auto_now=False, null=True)
    number_of_billing_successful = models.IntegerField(default=1)
    free_vaporizer = models.BooleanField(default=True)
    number_of_free_pipe = models.IntegerField(default=0)
    cancelled = models.BooleanField(default=False)
    #transaction information
    VPSProtocol = models.CharField(max_length = 20)
    TxType = models.CharField(max_length = 100)
    VendorTxCode = models.CharField(max_length = 100)
    Vendor = models.CharField(max_length = 100)
    Amount = models.CharField(max_length = 100)
    Currency = models.CharField(max_length = 100)
    Description = models.CharField(max_length = 255)
    BillingSurname = models.CharField(max_length = 100)
    BillingFirstnames = models.CharField(max_length = 100)
    BillingAddress1 = models.CharField(max_length = 100)
    BillingCity = models.CharField(max_length = 100)
    BillingPostCode = models.CharField(max_length = 100)
    BillingCountry = models.CharField(max_length = 100)
    DeliverySurname = models.CharField(max_length = 100)
    DeliveryFirstnames = models.CharField(max_length = 100)
    DeliveryAddress1 = models.CharField(max_length = 100)
    DeliveryCity = models.CharField(max_length = 100)
    DeliveryPostCode = models.CharField(max_length = 100)
    DeliveryCountry = models.CharField(max_length = 100)
    NotificationURL = models.CharField(max_length = 100)
    CreateToken = models.IntegerField(default = 0)
    error_detail = models.TextField(max_length=800, null=True, blank=True)

    freeze = models.BooleanField(default=False)
    sequence_strength = models.ForeignKey(StrengthSequence)




class AppSettings(models.Model):
    cigarette_price = models.FloatField(default=0.35)

    def __unicode__(self):
        return 'Users Admin Settings'
