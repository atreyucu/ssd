# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'StandingOrderInformation.freeze'
        db.add_column(u'users_standingorderinformation', 'freeze',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'StandingOrderInformation.sequence_strength'
        db.add_column(u'users_standingorderinformation', 'sequence_strength',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['admins.StrengthSequence']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'StandingOrderInformation.freeze'
        db.delete_column(u'users_standingorderinformation', 'freeze')

        # Deleting field 'StandingOrderInformation.sequence_strength'
        db.delete_column(u'users_standingorderinformation', 'sequence_strength_id')


    models = {
        u'admins.productsize': {
            'Meta': {'object_name': 'ProductSize'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'admins.strengthsequence': {
            'Meta': {'object_name': 'StrengthSequence'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_number': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'strength_1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_1_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_10': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_10_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_2_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_3': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_3_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_4': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_4_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_5': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_5_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_6': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_6_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_7': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_7_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_8': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_8_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_9': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_9_size'", 'to': u"orm['admins.ProductSize']"})
        },
        u'users.appsettings': {
            'Meta': {'object_name': 'AppSettings'},
            'cigarette_price': ('django.db.models.fields.FloatField', [], {'default': '0.35'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'users.standingorderinformation': {
            'Amount': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingAddress1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingCity': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingCountry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingFirstnames': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingPostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingSurname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'CreateToken': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'Currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryAddress1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryCity': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryCountry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryFirstnames': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryPostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliverySurname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'Meta': {'object_name': 'StandingOrderInformation'},
            'NotificationURL': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TxType': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'VPSProtocol': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Vendor': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'VendorTxCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'delivery_period': ('django.db.models.fields.IntegerField', [], {'default': '56'}),
            'error_detail': ('django.db.models.fields.TextField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'}),
            'free_vaporizer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'freeze': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_delivery_batch_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'last_delivery_printed': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'last_success_rebilling_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'next_delivery_printed': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'next_rebilling': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'number_of_billing_successful': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'number_of_free_pipe': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'order_date': ('django.db.models.fields.DateTimeField', [], {}),
            'owner_user_id': ('django.db.models.fields.IntegerField', [], {}),
            'rebilling_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'rebilling_period': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'sequence_strength': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['admins.StrengthSequence']"}),
            'unsubscribe_approved': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'unsubscribe_recived': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        }
    }

    complete_apps = ['users']