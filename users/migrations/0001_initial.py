# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StandingOrderInformation'
        db.create_table(u'users_standingorderinformation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('owner_user_id', self.gf('django.db.models.fields.IntegerField')()),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('rebilling_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('order_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('last_success_rebilling_date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('rebilling_period', self.gf('django.db.models.fields.IntegerField')(default=30)),
            ('next_rebilling', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('delivery_period', self.gf('django.db.models.fields.IntegerField')(default=56)),
            ('next_delivery_printed', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('last_delivery_printed', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('last_delivery_batch_id', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('unsubscribe_recived', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('unsubscribe_approved', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('number_of_billing_successful', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('free_vaporizer', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('number_of_free_pipe', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('cancelled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('VPSProtocol', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('TxType', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('VendorTxCode', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Vendor', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Amount', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Currency', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('BillingSurname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('BillingFirstnames', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('BillingAddress1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('BillingCity', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('BillingPostCode', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('BillingCountry', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DeliverySurname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DeliveryFirstnames', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DeliveryAddress1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DeliveryCity', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DeliveryPostCode', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DeliveryCountry', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('NotificationURL', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('CreateToken', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('error_detail', self.gf('django.db.models.fields.TextField')(max_length=800, null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['StandingOrderInformation'])

        # Adding model 'AppSettings'
        db.create_table(u'users_appsettings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cigarette_price', self.gf('django.db.models.fields.FloatField')(default=0.35)),
        ))
        db.send_create_signal(u'users', ['AppSettings'])


    def backwards(self, orm):
        # Deleting model 'StandingOrderInformation'
        db.delete_table(u'users_standingorderinformation')

        # Deleting model 'AppSettings'
        db.delete_table(u'users_appsettings')


    models = {
        u'users.appsettings': {
            'Meta': {'object_name': 'AppSettings'},
            'cigarette_price': ('django.db.models.fields.FloatField', [], {'default': '0.35'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'users.standingorderinformation': {
            'Amount': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingAddress1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingCity': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingCountry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingFirstnames': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingPostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingSurname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'CreateToken': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'Currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryAddress1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryCity': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryCountry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryFirstnames': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryPostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliverySurname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'Meta': {'object_name': 'StandingOrderInformation'},
            'NotificationURL': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TxType': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'VPSProtocol': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Vendor': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'VendorTxCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'delivery_period': ('django.db.models.fields.IntegerField', [], {'default': '56'}),
            'error_detail': ('django.db.models.fields.TextField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'}),
            'free_vaporizer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_delivery_batch_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'last_delivery_printed': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'last_success_rebilling_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'next_delivery_printed': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'next_rebilling': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'number_of_billing_successful': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'number_of_free_pipe': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'order_date': ('django.db.models.fields.DateTimeField', [], {}),
            'owner_user_id': ('django.db.models.fields.IntegerField', [], {}),
            'rebilling_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'rebilling_period': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'unsubscribe_approved': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'unsubscribe_recived': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        }
    }

    complete_apps = ['users']