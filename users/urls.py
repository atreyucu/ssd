from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'users.views.home'),
   (r'^yourdetails/$', 'users.views.your_details'),
   (r'^yoursubscriptation/$', 'users.views.your_subscription'),
   (r'^save_your_details/$', 'users.views.save_your_details'),
   (r'^contactus/$', 'users.views.contact_us'),
   (r'^save_conact_us/$', 'users.views.save_conact_us'),
   (r'^get_towns/$', 'users.views.get_towns'),
   (r'^change_flavour/$', 'users.views.change_flavour'),

    (r'^change_status_order/$', 'users.views.change_status_order'),
)