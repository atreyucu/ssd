from datetime import datetime, timedelta
import json
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.http.response import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import permission_required, login_required

from django.template.context import RequestContext
from admins.models import Message, SsdProductExtension
from cur.models import CountryCode, TownCode, UsersTitles
import general_auth
from general_auth.models import GeneralUser, Flavours

from ssd import settings
from social_invites.models import InviteRegister
from users.models import StandingOrderInformation

@permission_required('general_auth.can_access_useradmins')
def home(request):
    return render_to_response('useradmins/useradmins_home.html', context_instance=RequestContext(request))

@permission_required('general_auth.can_access_useradmins')
def your_subscription(request):
    list = range(1,6)
    user = request.user
    try:
        general_user = user.generaluser
    except GeneralUser.DoesNotExist:
        return HttpResponse('Error. Contact with your administrator')
    all_flavours = SsdProductExtension().get_all_products()
    flavours_user_arr = general_user.flavours_set.all()
    flavours_return = []
    for f in all_flavours:
        flavours_temp = {}
        flavours_temp['sld_ext_id'] = f.id
        flavours_temp['size'] = f.size.name
        cur_product = f.getCurProduct()
        flavours_temp['item_name'] = ""
        if cur_product:
            flavours_temp['item_name'] = cur_product.item_name + ' ' + f.size.short_name
        flavours_return.append(flavours_temp)

    if len(flavours_user_arr) > 0:
        flavours_user_temp = flavours_user_arr[0]
        flavours_user = {'flavour_1': flavours_user_temp.flavour_1, 'flavour_2': flavours_user_temp.flavour_2,
                        'flavour_3': flavours_user_temp.flavour_3, 'flavour_4': flavours_user_temp.flavour_4,
                        'flavour_5': flavours_user_temp.flavour_5}
    else:
        flavours_user = {'flavour_1': 0, 'flavour_2': 0,
                        'flavour_3': 0, 'flavour_4': 0,
                        'flavour_5': 0}


    standing_order_information = general_user.standing_order_information
    #standing_order_information = StandingOrderInformation()

    next_order_date = None
    next_billing_date = None
    if standing_order_information is not None:
        order_date = standing_order_information.order_date
        next_order_date = None
        next_billing_date = None
        if order_date is not None:
            next_order_date = order_date + timedelta(days=standing_order_information.delivery_period)
            next_billing_date =standing_order_information.next_rebilling

    if next_order_date is not None:
        next_order_date = next_order_date.strftime('%d/%m/%Y')
    if next_billing_date is not None:
        next_billing_date = next_billing_date.strftime('%d/%m/%Y')
    #invitation last month
    date_now = datetime.now()
    last_month = date_now - timedelta(days=30)
    invite_register = InviteRegister.objects.filter(reply_date__gte = last_month).filter(reply_date__lte = date_now).filter(is_follow=1).filter(is_sign_up=1)

    order_is_cancelled = standing_order_information.cancelled
    return render_to_response('useradmins/useradmins_subscriptions.html', {'order_is_cancelled':order_is_cancelled, 'vendor_name': settings.SAGE_VENDOR_NAME, 'amount_invites': len(invite_register), 'earned_pipes': len(invite_register), 'earned_ecigarettes': len(invite_register), 'next_order': next_order_date,'next_billing': next_billing_date, 'list':list, 'flavours_user': flavours_user, 'flavours': flavours_return, 'cigarret_per_day': general_user.cigarette_smoked_per_day}, context_instance=RequestContext(request))

@permission_required('general_auth.can_access_useradmins')
def change_status_order(request):
    user = request.user
    general_user = user.generaluser
    standing_order_information = general_user.standing_order_information
    data = {'success': True, 'text': ''}
    if standing_order_information.cancelled:
        standing_order_information.cancelled = False
        data['text'] = '(click to cancel)'
    else:
        standing_order_information.cancelled = True
        data['text'] = '(click to activate)'
    standing_order_information.save()
    return HttpResponse(json.dumps(data), mimetype='application/json')



@permission_required('general_auth.can_access_useradmins')
def your_details(request):
    user = request.user
    generaluser = request.user.generaluser
    contries = CountryCode.objects.filter(available=1)

    your_towns = TownCode.objects.filter(countryid=generaluser.your_country)
    delivery_towns = TownCode.objects.filter(countryid=generaluser.delivery_country)
    billing_towns = TownCode.objects.filter(countryid=generaluser.billing_country)

    titles = UsersTitles.objects.all()
    return render_to_response('useradmins/useradmins_your_details.html',
                              {'contries': contries, 'titles': titles, 'generaluser': generaluser,
                               'your_towns': your_towns, 'delivery_towns': delivery_towns,
                               'billing_towns': billing_towns}, context_instance=RequestContext(request))


def get_towns(request):
    country = request.GET['country']
    towns = TownCode.objects.filter(countryid=country)
    dict_towns = {}
    for town in towns:
        dict_towns[town.codeid] = town.code_name
    return HttpResponse(json.dumps(dict_towns), mimetype='application/json')

@permission_required('general_auth.can_access_useradmins')
def save_your_details(request):
    generaluser = request.user.generaluser

    generaluser.email = request.POST['email']
    generaluser.password = request.POST['password']

    generaluser.your_title = request.POST['your_title']
    generaluser.your_firstname = request.POST['your_firstname']
    generaluser.your_secondname = request.POST['your_secondname']
    generaluser.your_hbn = request.POST['your_hbn']
    generaluser.your_streetaddress = request.POST['your_streetaddress']
    generaluser.your_secondlineaddress = request.POST['your_secondlineaddress']
    generaluser.your_towncity = request.POST['your_towncity']
    generaluser.your_country = request.POST['your_country']
    generaluser.your_postcode = request.POST['your_postcode']

    generaluser.delivery_title = request.POST['delivery_title']
    generaluser.delivery_firstname = request.POST['delivery_firstname']
    generaluser.delivery_secondname = request.POST['delivery_secondname']
    generaluser.delivery_hbn = request.POST['delivery_hbn']
    generaluser.delivery_streetaddress = request.POST['delivery_streetaddress']
    generaluser.delivery_secondlineaddress = request.POST['delivery_secondlineaddress']
    generaluser.delivery_towncity = request.POST['delivery_towncity']
    generaluser.delivery_country = request.POST['delivery_country']
    generaluser.delivery_postcode = request.POST['delivery_postcode']

    generaluser.billing_title = request.POST['billing_title']
    generaluser.billing_firstname = request.POST['billing_firstname']
    generaluser.billing_secondname = request.POST['billing_secondname']
    generaluser.billing_hbn = request.POST['billing_hbn']
    generaluser.billing_streetaddress = request.POST['billing_streetaddress']
    generaluser.billing_secondlineaddress = request.POST['billing_secondlineaddress']
    generaluser.billing_towncity = request.POST['billing_towncity']
    generaluser.billing_country = request.POST['billing_country']
    generaluser.billing_postcode = request.POST['billing_postcode']

    generaluser.save()

    return redirect('/usersadmin/yourdetails/')

@permission_required('general_auth.can_access_useradmins')
def contact_us(request):
    return render_to_response('useradmins/useradmins_contact_us.html', context_instance=RequestContext(request))

@permission_required('general_auth.can_access_useradmins')
def save_conact_us(request):
    subject = request.POST['subject']
    body = request.POST['body']

    message = Message(user=request.user, subject=subject, body=body, sent=datetime.now())
    message.save()
    return redirect('/usersadmin/contactus/')


@permission_required('general_auth.can_access_useradmins')
def change_flavour(request):
    if request.GET.has_key('flavour') and request.GET.has_key('number'):
        flavour = request.GET.get('flavour')
        number = request.GET.get('number')
        general_user = request.user.generaluser
        flavour_user = Flavours.objects.get(general_user =  general_user)
        flavour_user.change_flavour(flavour=flavour, number=number)
        data = {'success': True}
    else:
        data = {'success':False}
    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type='application/json')

