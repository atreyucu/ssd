from __future__ import unicode_literals

from django.db import models
from django.contrib.admin import site

class Brand(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255L)
    logo = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'brand'

class ClientModuleConfiguration(models.Model):
    id = models.IntegerField(primary_key=True)
    supplier_id = models.IntegerField()
    conf_category = models.ForeignKey('ConfigurationCategory')
    is_locked = models.IntegerField()
    name = models.CharField(max_length=255L)
    value = models.TextField(blank=True)
    slug = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'client_module_configuration'

class CommissionRate(models.Model):
    id = models.IntegerField(primary_key=True)
    product_category_group = models.ForeignKey('ProductCategoryGroup')
    supplierid = models.IntegerField()
    minimun_value = models.FloatField(null=True, blank=True)
    seller_value = models.FloatField(null=True, blank=True)
    is_inhouse = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'commission_rate'

class Configuration(models.Model):
    id = models.BigIntegerField(primary_key=True)
    onesite4fashion_userid = models.IntegerField(null=True, blank=True)
    product_purchase_confirmation = models.TextField(blank=True)
    product_delivery_confirmation = models.TextField(blank=True)
    product_experience = models.TextField(blank=True)
    class Meta:
        db_table = 'configuration'

class ConfigurationCategory(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255L, unique=True)
    description = models.TextField(blank=True)
    slug = models.CharField(max_length=255L, unique=True, blank=True)
    class Meta:
        db_table = 'configuration_category'

class DeliveryOption(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255L, blank=True)
    type = models.IntegerField(null=True, blank=True)
    slug = models.CharField(max_length=50L, blank=True)
    class Meta:
        db_table = 'delivery_option'

class InvoiceReference(models.Model):
    id = models.BigIntegerField(primary_key=True)
    supplier_id = models.IntegerField(null=True, blank=True)
    amount_due = models.FloatField(null=True, blank=True)
    due_date = models.DateTimeField(null=True, blank=True)
    date_first_order = models.DateTimeField(null=True, blank=True)
    date_last_order = models.DateTimeField(null=True, blank=True)
    our_vat_number = models.CharField(max_length=255L, blank=True)
    status = models.IntegerField(null=True, blank=True)
    included_orders = models.TextField(blank=True)
    created_at = models.DateTimeField()
    class Meta:
        db_table = 'invoice_reference'

class MeasurementName(models.Model):
    id = models.IntegerField(primary_key=True)
    measurement1 = models.CharField(max_length=255L, blank=True)
    measurement2 = models.CharField(max_length=255L, blank=True)
    measurement3 = models.CharField(max_length=255L, blank=True)
    measurement4 = models.CharField(max_length=255L, blank=True)
    measurement5 = models.CharField(max_length=255L, blank=True)
    measurement6 = models.CharField(max_length=255L, blank=True)
    product_category = models.ForeignKey('ProductCategory', null=True, blank=True)
    class Meta:
        db_table = 'measurement_name'

class MeasurementUnit(models.Model):
    id = models.IntegerField(primary_key=True)
    unit = models.CharField(max_length=10L, blank=True)
    supplier_id = models.IntegerField()
    class Meta:
        db_table = 'measurement_unit'

class MeasurementValue(models.Model):
    id = models.IntegerField(primary_key=True)
    measurement1 = models.CharField(max_length=255L, blank=True)
    measurement2 = models.CharField(max_length=255L, blank=True)
    measurement3 = models.CharField(max_length=255L, blank=True)
    measurement4 = models.CharField(max_length=255L, blank=True)
    measurement5 = models.CharField(max_length=255L, blank=True)
    measurement6 = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'measurement_value'

class ModuleErrorCode(models.Model):
    payment_option_id = models.IntegerField(primary_key=True)
    error_code = models.CharField(max_length=255L, blank=True)
    payment_procesor_error = models.TextField(blank=True)
    payment_error_messasge = models.TextField(blank=True)
    class Meta:
        db_table = 'module_error_code'

class OrderExtension(models.Model):
    id = models.BigIntegerField(primary_key=True)
    tran_id = models.IntegerField()
    order_id = models.IntegerField(null=True, blank=True)
    order_number = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'order_extension'

class PaymentOption(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255L, blank=True)
    slug = models.CharField(max_length=50L, blank=True)
    module_url = models.CharField(max_length=255L, blank=True)
    method_type = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'payment_option'

class ProductCategory(models.Model):
    id = models.IntegerField(primary_key=True)
    product_category_group = models.ForeignKey('ProductCategoryGroup')
    name = models.CharField(max_length=255L)
    slug = models.CharField(max_length=255L, unique=True, blank=True)
    class Meta:
        db_table = 'product_category'

class ProductCategoryGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255L, blank=True)
    target_gender = models.IntegerField(null=True, blank=True)
    slug = models.CharField(max_length=255L, unique=True, blank=True)
    class Meta:
        db_table = 'product_category_group'

class ProductCategorySize(models.Model):
    product_size = models.ForeignKey('ProductSize')
    product_category = models.ForeignKey(ProductCategory)
    class Meta:
        db_table = 'product_category_size'

class ProductExtension(models.Model):
    id = models.IntegerField(primary_key=True)
    size_guide = models.CharField(max_length=255L, blank=True)
    product_id = models.IntegerField()
    parent_product_id = models.IntegerField(null=True, blank=True)
    product_size = models.ForeignKey('ProductSize', null=True, blank=True)
    product_category = models.ForeignKey(ProductCategory, null=True, blank=True)
    product_family = models.ForeignKey('ProductFamily', null=True, blank=True)
    product_seller_id = models.IntegerField(null=True, blank=True)
    parent_product_seller_id = models.IntegerField(null=True, blank=True)
    brand = models.ForeignKey(Brand, null=True, blank=True)
    measurement = models.ForeignKey(MeasurementValue, null=True, blank=True)
    over50age_group = models.IntegerField(null=True, blank=True)
    is_exclusive = models.IntegerField()
    color = models.CharField(max_length=255L, blank=True)
    picture1_url = models.CharField(max_length=255L, blank=True)
    picture2_url = models.CharField(max_length=255L, blank=True)
    picture3_url = models.CharField(max_length=255L, blank=True)
    video_url = models.CharField(max_length=255L, blank=True)
    updated_at = models.DateTimeField()
    is_leader = models.IntegerField(null=True, blank=True)
    is_deleted = models.IntegerField()
    is_active = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'product_extension'

class ProductFamily(models.Model):
    id = models.IntegerField(primary_key=True)
    product_category = models.ForeignKey(ProductCategory)
    name = models.CharField(max_length=255L, blank=True)
    slug = models.CharField(max_length=255L, unique=True, blank=True)
    class Meta:
        db_table = 'product_family'

class ProductImage(models.Model):
    product_id = models.IntegerField(primary_key=True)
    picture1_large = models.CharField(max_length=255L, blank=True)
    picture2_large = models.CharField(max_length=255L, blank=True)
    picture3_large = models.CharField(max_length=255L, blank=True)
    picture1_product_page = models.CharField(max_length=255L, blank=True)
    picture2_product_page = models.CharField(max_length=255L, blank=True)
    picture3_product_page = models.CharField(max_length=255L, blank=True)
    picture1_product_page_thumb = models.CharField(max_length=255L, blank=True)
    picture2_product_page_thumb = models.CharField(max_length=255L, blank=True)
    picture3_product_page_thumb = models.CharField(max_length=255L, blank=True)
    picture1_thumbnail = models.CharField(max_length=255L, blank=True)
    picture2_thumbnail = models.CharField(max_length=255L, blank=True)
    picture3_thumbnail = models.CharField(max_length=255L, blank=True)
    picture1_category = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'product_image'

class ProductLog(models.Model):
    id = models.IntegerField(primary_key=True)
    product_id = models.IntegerField(null=True, blank=True)
    field_name = models.CharField(max_length=255L, blank=True)
    local_value = models.CharField(max_length=255L, blank=True)
    seller_value = models.TextField(blank=True)
    created_at = models.DateTimeField()
    class Meta:
        db_table = 'product_log'

class ProductSize(models.Model):
    id = models.IntegerField(primary_key=True)
    value = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'product_size'

class Ranking(models.Model):
    id = models.IntegerField(primary_key=True)
    supplierid = models.IntegerField(null=True, blank=True)
    points = models.IntegerField(null=True, blank=True)
    order_count = models.IntegerField(null=True, blank=True)
    sales_value = models.FloatField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'ranking'

class RankingData(models.Model):
    id = models.IntegerField(primary_key=True)
    supplierid = models.IntegerField(null=True, blank=True)
    external_points = models.IntegerField(null=True, blank=True)
    inhouse_points = models.IntegerField(null=True, blank=True)
    online_offering_points = models.IntegerField(null=True, blank=True)
    delivery_cost_points = models.IntegerField(null=True, blank=True)
    price_charged_points = models.IntegerField(null=True, blank=True)
    invitation_points = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'ranking_data'

class RetailerDeliveryOption(models.Model):
    delivery_option = models.ForeignKey(DeliveryOption)
    supplierid = models.IntegerField()
    delivery_cost = models.FloatField(null=True, blank=True)
    delivery_vat_amount = models.FloatField(null=True, blank=True)
    delivery_free_over_amount = models.FloatField(null=True, blank=True)
    class Meta:
        db_table = 'retailer_delivery_option'

class RetailerPaymentOption(models.Model):
    payment_option = models.ForeignKey(PaymentOption)
    supplierid = models.IntegerField()
    is_active = models.IntegerField(null=True, blank=True)
    flexi_field1 = models.CharField(max_length=255L, blank=True)
    flexi_field2 = models.CharField(max_length=255L, blank=True)
    flexi_field3 = models.CharField(max_length=255L, blank=True)
    flexi_field4 = models.CharField(max_length=255L, blank=True)
    flexi_field5 = models.CharField(max_length=255L, blank=True)
    class Meta:
        db_table = 'retailer_payment_option'

class UserHistoryLog(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField(null=True, blank=True)
    user_name = models.CharField(max_length=255L, blank=True)
    description = models.TextField(blank=True)
    source_ip = models.TextField(blank=True)
    created_at = models.DateTimeField()
    class Meta:
        db_table = 'user_history_log'

class VatLog(models.Model):
    id = models.IntegerField(primary_key=True)
    productid = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True)
    class Meta:
        db_table = 'vat_log'

site.register(Brand)
site.register(ClientModuleConfiguration)
site.register(CommissionRate)
site.register(Configuration)
site.register(ConfigurationCategory)
site.register(DeliveryOption)
site.register(InvoiceReference)
site.register(MeasurementName)
site.register(MeasurementUnit)
site.register(MeasurementValue)
site.register(ModuleErrorCode)
site.register(OrderExtension)
site.register(PaymentOption)
site.register(ProductCategory)
site.register(ProductCategoryGroup)
site.register(ProductCategorySize)
site.register(ProductExtension)
site.register(ProductFamily)
site.register(ProductImage)
site.register(ProductLog)
site.register(ProductSize)
site.register(Ranking)
site.register(RankingData)
site.register(RetailerDeliveryOption)
site.register(RetailerPaymentOption)
site.register(UserHistoryLog)
site.register(VatLog)