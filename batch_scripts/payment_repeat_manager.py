#!/usr/bin/env python
import os
import sys
import datetime
from datetime import timedelta



SLD_PATH = "/home/ernesto/PycharmProjects/sld/"
APPS_PATH = ""

if __name__ == "__main__":
    sys.path.append(SLD_PATH)
    sys.path.append(APPS_PATH)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sld.settings")

    from users.models import StandingOrderInformation
    import ssd.settings as settings
    from django_sagepay.utils import encode_transaction_request, decode_transaction_response
    import requests
    from requests.exceptions import RequestException
    from cor.models import Orderr, Item
    from cur.models import Users,Customers
    from admins.models import PrintedOrders

    soi_list = StandingOrderInformation.objects.exclude(cancelled=True)

    today_date = datetime.date.today()

    for soi in soi_list:
        current_rebilling_period = soi.last_success_rebilling_date.date() - datetime.date.today()

        #Si ya es momento de repetir el pago para este usuario se repite el pago
        if current_rebilling_period.days > settings.PAYMENT_REPEAT_REBILLING_PERIOD:
            data = {}
            data['VPSProtocol'] = soi.VPSProtocol
            data['TxType'] = soi.TxType
            data['VendorTxCode'] = soi.VendorTxCode
            data['Vendor'] = soi.Vendor
            data['Amount'] = float(soi.Amount)
            data['Currency'] = soi.Currency
            data['Description'] = soi.Description
            data['BillingSurname'] = soi.BillingSurname
            data['BillingFirstnames'] = soi.BillingFirstnames
            data['BillingAddress1'] = soi.BillingAddress1
            data['BillingCity'] = soi.BillingCity
            data['BillingPostCode'] = soi.BillingPostCode
            data['BillingCountry'] = soi.BillingCountry
            data['DeliverySurname'] = soi.DeliverySurname
            data['DeliveryFirstnames'] = soi.DeliveryFirstnames
            data['DeliveryAddress1'] = soi.DeliveryAddress1
            data['DeliveryCity'] = soi.DeliveryCity
            data['DeliveryPostCode'] = soi.DeliveryPostCode
            data['DeliveryCountry'] = soi.DeliveryCountry
            data['NotificationURL'] = soi.NotificationURL
            data['CreateToken'] = 1

            request_body = encode_transaction_request(data)

            try:
                # Fire request to SagePay which creates the transaction
                response = requests.post(settings.SAGEPAY_URL, data=request_body,
                                         headers={"Content-Type": "application/x-www-form-urlencoded"})
                # Does nothing on 200, but raises exceptions for other statuses
                response.raise_for_status()
                # RequestException covers network/DNS related problems as well as non-200
                # responses
            except RequestException as e:
                soi.error_detail = e.message


            response_data = decode_transaction_response(response.text)

            if response_data['Status'] == 'OK REPEATED':
                try:
                    soi.rebilling_id = response_data['VPSTxId']
                except Exception, ex:
                    soi.error_detail = e.message


            # Si se cumle que ya es momento de enviar una orden pues se envia la orden y se pone como printable
            current_delivery_period = soi.last_delivery_printed.date() - datetime.date.now()

            if current_rebilling_period.days > settings.PAYMENT_REPEAT_DELIVERY_PERIOD:
                # OJO aqui generar una orden para esta trasaccion
                order = Orderr()
                #customer_users = Customers()
                cur_user = Users.objects.get(user_id=soi.generaluser.cur_user_id)
                customer_users_arr = Customers().get_customers_by_futurefour_id(cur_user.futurefour_id)
                sld_user = Users().get_ssd_user()
                for cu in customer_users_arr:
                        if cu.owner_userid == sld_user.userid:
                            customer_users = cu
                            exists_customer = True
                            break
                order.customer_id = cur_user.userid
                order.customer_idn_number = 0
                order.customer_futurefour_id = customer_users.futurefour_id
                order.customer_number = customer_users.customerid
                order.customer_business_name = customer_users.contact1_firstname + ' ' + customer_users.contact1_lastname
                order.customer_phone = customer_users.contact1_mobile_phone_number
                order.customer_mobile = customer_users.contact1_mobile_phone_number
                order.customer_email = customer_users.contact1_confirmed_email
                order.customer_fax = customer_users.contact1_fax_number

                order.application_signature = 'SLD'
                order.delivery_cost = 0
                order.delivery_vat = 0
                order.order_total = settings.SLD_MONTH_ORDER_AMMOUNT
                order.order_total = settings.SLD_MONTH_ORDER_AMMOUNT


                order.deliver_order_to_address_hbn = customer_users.customer_address_hbn
                order.deliver_to_address_street = customer_users.customer_address_street
                order.deliver_to_address_town = customer_users.customer_address_town
                order.deliver_to_address_city = customer_users.customer_address_city
                order.deliver_to_address_country = customer_users.customer_address_country
                order.deliver_to_address_county = customer_users.customer_address_county
                order.deliver_to_address_postcode = customer_users.customer_address_postcode

                order.billing_order_to_address_hbn = customer_users.billing_address_hbn
                order.billing_to_address_street = customer_users.billing_address_street
                order.billing_to_address_town = customer_users.billing_address_town
                order.billing_to_address_city = customer_users.billing_address_city
                order.billing_to_address_country = customer_users.billing_address_country
                order.billing_to_address_county = customer_users.billing_address_county
                order.billing_to_address_postcode = customer_users.billing_address_postcode

                import time
                order.order_generation_date = time.time()
                order.save()

                #item para el flavour 1

                user_flavours = soi.generaluser.flavours_set.all()[0]

                sld = user_flavours.get_sld_flavour_1()
                item = Item()
                item.tran = order
                cur_product = sld.getCurProduct()
                item.supplier_item_name = cur_product.item_name
                item.supplier_item_number = cur_product.item_number
                item.supplier_product_id = cur_product.productid
                #ver si hay que cambiar esto
                item.supplier_net = 0
                item.supplier_gross = 0
                item.supplier_vat_rate = 0
                item.supplier_vat_value = 0
                item.save()

                #item para el flavour 2

                sld = user_flavours.get_sld_flavour_2()
                item = Item()
                item.tran = order
                cur_product = sld.getCurProduct()
                item.supplier_item_name = cur_product.item_name
                item.supplier_item_number = cur_product.item_number
                item.supplier_product_id = cur_product.productid
                #ver si hay que cambiar esto
                item.supplier_net = 0
                item.supplier_gross = 0
                item.supplier_vat_rate = 0
                item.supplier_vat_value = 0
                item.save()

                            #item para el flavour 3

                sld = user_flavours.get_sld_flavour_3()
                item = Item()
                item.tran = order
                cur_product = sld.getCurProduct()
                item.supplier_item_name = cur_product.item_name
                item.supplier_item_number = cur_product.item_number
                item.supplier_product_id = cur_product.productid
                #ver si hay que cambiar esto
                item.supplier_net = 0
                item.supplier_gross = 0
                item.supplier_vat_rate = 0
                item.supplier_vat_value = 0
                item.save()

                            #item para el flavour 4

                sld = user_flavours.get_sld_flavour_4()
                item = Item()
                item.tran = order
                cur_product = sld.getCurProduct()
                item.supplier_item_name = cur_product.item_name
                item.supplier_item_number = cur_product.item_number
                item.supplier_product_id = cur_product.productid
                #ver si hay que cambiar esto
                item.supplier_net = 0
                item.supplier_gross = 0
                item.supplier_vat_rate = 0
                item.supplier_vat_value = 0
                item.save()

                            #item para el flavour 5

                sld = user_flavours.get_sld_flavour_5()
                item = Item()
                item.tran = order
                cur_product = sld.getCurProduct()
                item.supplier_item_name = cur_product.item_name
                item.supplier_item_number = cur_product.item_number
                item.supplier_product_id = cur_product.productid
                #ver si hay que cambiar esto
                item.supplier_net = 0
                item.supplier_gross = 0
                item.supplier_vat_rate = 0
                item.supplier_vat_value = 0
                item.save()

                printed_order = PrintedOrders()
                printed_order.order_id = order.tran_id
                printed_order.save()

                soi.last_delivery_batch_id = printed_order.id
                soi.save()
