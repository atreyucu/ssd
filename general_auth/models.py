from django.db import models
from django.contrib.auth.models import User
from admins.models import SsdProductExtension
from cur.models import Users, Products
from users.models import StandingOrderInformation


class GeneralUser(models.Model):
    user = models.OneToOneField(User)
    cur_user_id = models.IntegerField(default = 0, null=False)

    standing_order_information = models.OneToOneField(StandingOrderInformation, default=None, null=True)
    cigarette_smoked_per_day = models.IntegerField(default=1)

    email = models.EmailField(max_length=70, null=False, blank=True)
    password = models.CharField(max_length=150, null=False, blank=True)

    your_title = models.IntegerField(default = 0, null=False, blank=False)
    your_firstname = models.CharField(max_length=150, null=False, blank=True)
    your_secondname = models.CharField(max_length=150, null=False, blank=True)
    your_hbn = models.CharField(max_length=150, null=False, blank=True)
    your_streetaddress = models.CharField(max_length=150, null=False, blank=True)
    your_secondlineaddress = models.CharField(max_length=150, null=False, blank=True)
    your_towncity = models.IntegerField(default = 0, null=False, blank=False)
    your_country = models.CharField(max_length = 10, default="", null=False, blank=False)
    your_postcode = models.CharField(max_length=150, null=False, blank=True)

    delivery_title = models.IntegerField(default = 0, null=False, blank=False)
    delivery_firstname = models.CharField(max_length=150, null=False, blank=True)
    delivery_secondname = models.CharField(max_length=150, null=False, blank=True)
    delivery_hbn = models.CharField(max_length=150, null=False, blank=True)
    delivery_streetaddress = models.CharField(max_length=150, null=False, blank=True)
    delivery_secondlineaddress = models.CharField(max_length=150, null=False, blank=True)
    delivery_towncity = models.IntegerField(default = 0, null=False, blank=False)
    delivery_country = models.CharField(max_length = 10, default="", null=False, blank=False)
    delivery_postcode = models.CharField(max_length=150, null=False, blank=True)

    billing_title = models.IntegerField(default = 0, null=False, blank=False)
    billing_firstname = models.CharField(max_length=150, null=False, blank=True)
    billing_secondname = models.CharField(max_length=150, null=False, blank=True)
    billing_hbn = models.CharField(max_length=150, null=False, blank=True)
    billing_streetaddress = models.CharField(max_length=150, null=False, blank=True)
    billing_secondlineaddress = models.CharField(max_length=150, null=False, blank=True)
    billing_towncity = models.IntegerField(default = 0, null=False, blank=False)
    billing_country = models.CharField(max_length = 10, default="", null=False, blank=False)
    billing_postcode = models.CharField(max_length=150, null=False, blank=True)


    class Meta:
        permissions = (
                ("can_access_useradmins", "Access to YourAdmins"),
            ("can_access_ouradmins", "Access to OurAdmins"),
            ("can_access_sld", "Access to AdminInterface"),
        )

    def getCurUser(self):
        try:
            return Users.objects.get(userid=self.cur_user_id)
        except Users.DoesNotExist:
            return None
    def get_guser_by_email(self, email):
        try:
            return GeneralUser.objects.get(email=email)
        except GeneralUser.DoesNotExist:
            return None

class Flavours(models.Model):
    flavour_1 = models.IntegerField()
    general_user = models.ForeignKey(GeneralUser)

    def get_sld_flavour_1(self):
        try:
            return SsdProductExtension.objects.get(id = self.flavour_1)
        except SsdProductExtension.DoesNotExist:
            return None
