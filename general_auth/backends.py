from django.contrib.auth.models import User, check_password
from cur.models import Users

class GeneralAuthBackend(object):
    def authenticate(self, username=None, password=None):
        valid = Users.user_manager.check_credentials(username,password)
        if valid:
            try:
                user = User.objects.get(username=username)
                users_instance = Users.objects.get(username=username,password=password)
                user.set_password(users_instance.password)
                user.first_name = users_instance.firstname
                user.last_name = users_instance.lastname
                user.email = users_instance.email_address
                user.save()
                return user
            except User.DoesNotExist:
                return None
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


