# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GeneralUser.cigarette_smoked_per_day'
        db.add_column(u'general_auth_generaluser', 'cigarette_smoked_per_day',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'GeneralUser.cigarette_smoked_per_day'
        db.delete_column(u'general_auth_generaluser', 'cigarette_smoked_per_day')


    models = {
        u'admins.productsize': {
            'Meta': {'object_name': 'ProductSize'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'admins.strengthsequence': {
            'Meta': {'object_name': 'StrengthSequence'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_number': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'strength_1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_1_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_10': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_10_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_2_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_3': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_3_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_4': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_4_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_5': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_5_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_6': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_6_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_7': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_7_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_8': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_8_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_9': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_9_size'", 'to': u"orm['admins.ProductSize']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'general_auth.flavours': {
            'Meta': {'object_name': 'Flavours'},
            'flavour_1': ('django.db.models.fields.IntegerField', [], {}),
            'general_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['general_auth.GeneralUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'general_auth.generaluser': {
            'Meta': {'object_name': 'GeneralUser'},
            'billing_country': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'billing_firstname': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'billing_hbn': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'billing_postcode': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'billing_secondlineaddress': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'billing_secondname': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'billing_streetaddress': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'billing_title': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'billing_towncity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cigarette_smoked_per_day': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'cur_user_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'delivery_country': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'delivery_firstname': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'delivery_hbn': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'delivery_postcode': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'delivery_secondlineaddress': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'delivery_secondname': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'delivery_streetaddress': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'delivery_title': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'delivery_towncity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '70', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'standing_order_information': ('django.db.models.fields.related.OneToOneField', [], {'default': 'None', 'to': u"orm['users.StandingOrderInformation']", 'unique': 'True', 'null': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'your_country': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'your_firstname': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'your_hbn': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'your_postcode': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'your_secondlineaddress': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'your_secondname': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'your_streetaddress': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'your_title': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'your_towncity': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'users.standingorderinformation': {
            'Amount': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingAddress1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingCity': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingCountry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingFirstnames': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingPostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'BillingSurname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'CreateToken': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'Currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryAddress1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryCity': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryCountry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryFirstnames': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliveryPostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'DeliverySurname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'Meta': {'object_name': 'StandingOrderInformation'},
            'NotificationURL': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TxType': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'VPSProtocol': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Vendor': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'VendorTxCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'delivery_period': ('django.db.models.fields.IntegerField', [], {'default': '56'}),
            'error_detail': ('django.db.models.fields.TextField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'}),
            'free_vaporizer': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'freeze': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_delivery_batch_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'last_delivery_printed': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'last_success_rebilling_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'next_delivery_printed': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'next_rebilling': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'number_of_billing_successful': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'number_of_free_pipe': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'order_date': ('django.db.models.fields.DateTimeField', [], {}),
            'owner_user_id': ('django.db.models.fields.IntegerField', [], {}),
            'rebilling_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'rebilling_period': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'sequence_strength': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['admins.StrengthSequence']"}),
            'unsubscribe_approved': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'unsubscribe_recived': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        }
    }

    complete_apps = ['general_auth']