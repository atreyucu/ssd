'''
This module contain the managers that implement extra
functional to the CUR models.
'''

from django.db.models import Manager

class CurUserManager(Manager):
    def check_credentials(self, user_name=None, password=None):
        '''check the credentials using the CUR Users table
           params:
              str user_name
              str password (without encrypt)
           return:
              bool value,
               true if the credentials are valid false other case
        '''
        if(user_name != None and password != None):
            try:
                from django.db import connections
                cursor = connections['cur'].cursor()
                query = "select * from users where users.username like '%s' and users.password like '%s'"%(user_name,password)
                cursor.execute(query)
                count=0
                for row in cursor.fetchall():
                    count+=1
                return count > 0
            except Exception:
                return False
        else:
            return False