from django.db import models
from django.contrib.admin import site
import managers
from social_graph.models import SfGuardUserProfile

class Affiliates(models.Model):
    affiliateid = models.IntegerField(primary_key=True)
    affiliate_number = models.TextField(blank=True)
    affiliate_deleted = models.IntegerField()
    affiliate_delete_date = models.CharField(max_length=765, blank=True)
    affiliate_owner_userid = models.IntegerField(null=True, blank=True)
    options_affiliate_group = models.IntegerField()
    affiliate_personal_title = models.IntegerField(null=True, blank=True)
    affiliate_personal_firstname = models.TextField(blank=True)
    affiliate_personal_lastname = models.TextField(blank=True)
    affiliate_personal_uncon_email = models.TextField(blank=True)
    affiliate_personal_conf_email = models.TextField(blank=True)
    affiliate_personal_conf_d_t_ip = models.TextField(blank=True)
    affiliates_personal_contact_information = models.TextField(blank=True)
    affiliate_company_businessname = models.TextField(blank=True)
    affiliate_company_vat_number = models.CharField(max_length=765, blank=True)
    affiliate_company_comp_number = models.TextField(blank=True)
    affiliate_address_hbn = models.TextField(blank=True)
    affiliate_address_street = models.TextField(blank=True)
    affiliate_address_town = models.TextField(blank=True)
    affiliate_address_city = models.TextField(blank=True)
    affiliate_address_county = models.TextField(blank=True)
    affiliate_address_country = models.TextField(blank=True)
    affiliate_address_postcode = models.TextField(blank=True)
    affiliate_address_pref_payment = models.TextField(blank=True)
    affiliate_address_telephone = models.TextField(blank=True)
    affiliates_address_mobile_phone = models.TextField(blank=True)
    affiliate_address_fax = models.TextField(blank=True)
    affiliate_address_website = models.TextField(blank=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    sta_identification_number = models.CharField(max_length=765, blank=True)
    supplier_business_name = models.TextField(blank=True)
    contact1_title = models.IntegerField(null=True, blank=True)
    contact1_firstname = models.TextField(blank=True)
    contact1_lastname = models.TextField(blank=True)
    contact1_job_title = models.TextField(blank=True)
    contact1_direct_telephone = models.TextField(blank=True)
    contact1_other_telephone = models.TextField(blank=True)
    contact1_mobile_phone_number = models.TextField(blank=True)
    contact1_fax_number = models.TextField(blank=True)
    contact1_unconfirmed_email = models.TextField(blank=True)
    contact1_confirmed_email = models.TextField(blank=True)
    contact1_confirmed_date_time_ip = models.TextField(blank=True)
    contact2_title = models.IntegerField(null=True, blank=True)
    contact2_firstname = models.TextField(blank=True)
    contact2_lastname = models.TextField(blank=True)
    contact2_job_title = models.TextField(blank=True)
    contact2_direct_telephone = models.TextField(blank=True)
    contact2_other_telephone = models.TextField(blank=True)
    contact2_mobile_phone_number = models.TextField(blank=True)
    contact2_fax_number = models.TextField(blank=True)
    contact2_unconfirmed_email = models.TextField(blank=True)
    contact2_confirmed_email = models.TextField(blank=True)
    contact2_confirmed_date_time_ip = models.TextField(blank=True)
    supplier_address_street = models.TextField(blank=True)
    supplier_address_town = models.TextField(blank=True)
    supplier_address_city = models.TextField(blank=True)
    supplier_address_county = models.TextField(blank=True)
    supplier_address_country = models.TextField(blank=True)
    supplier_address_postcode = models.TextField(blank=True)
    billing_address_street = models.TextField(blank=True)
    billing_address_town = models.TextField(blank=True)
    billing_address_city = models.TextField(blank=True)
    billing_address_county = models.TextField(blank=True)
    billing_address_country = models.TextField(blank=True)
    billing_address_postcode = models.TextField(blank=True)
    company_details_website = models.TextField(blank=True)
    commpany_details_vat = models.TextField(blank=True)
    company_details_customer_id = models.TextField(blank=True)
    class Meta:
        db_table = u'affiliates'

class ContactGroups(models.Model):
    groupid = models.IntegerField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    group_name = models.TextField(blank=True)
    group_type = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'contact_groups'

class CountryCode(models.Model):
    codeid = models.CharField(max_length=30, primary_key=True)
    code_name = models.CharField(max_length=765, blank=True)
    available = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'country_code'

class CreditCardDetails(models.Model):
    id = models.IntegerField(primary_key=True)
    credit_card_owner_userid = models.IntegerField()
    credit_card_owner_name = models.TextField(blank=True)
    credit_card_number = models.TextField(blank=True)
    credit_card_type = models.TextField(blank=True)
    credit_card_issued_date = models.DateTimeField(null=True, blank=True)
    credit_card_expiry_date = models.DateTimeField(null=True, blank=True)
    primary = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'credit_card_details'

class Customers(models.Model):
    customerid = models.AutoField(primary_key=True)
    customer_number = models.TextField(blank=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    sta_identification_number = models.CharField(max_length=765, blank=True)
    futurefour_id = models.CharField(max_length=765, blank=True)
    futurefour_username = models.TextField(blank=True)
    customer_deleted = models.IntegerField(default=0)
    customer_delete_date = models.CharField(max_length=765, blank=True)
    active_customer = models.IntegerField(default=1)
    company_business_name = models.TextField(blank=True)
    company_vat_number = models.CharField(max_length=765, blank=True)
    company_website = models.TextField(blank=True)
    company_sta_iman = models.TextField(blank=True)
    contact1_title = models.IntegerField(null=True, blank=True)
    contact1_firstname = models.TextField(blank=True)
    contact1_lastname = models.TextField(blank=True)
    contact1_job_title = models.TextField(blank=True)
    contact1_direct_telephone = models.TextField(blank=True)
    contact1_other_telephone = models.TextField(blank=True)
    contact1_mobile_phone_number = models.TextField(blank=True)
    contact1_fax_number = models.TextField(blank=True)
    contact1_unconfirmed_email = models.TextField(blank=True)
    contact1_confirmed_email = models.TextField(blank=True)
    contact1_confirmed_date_time_ip = models.TextField(blank=True)
    contact2_title = models.IntegerField(null=True, blank=True)
    contact2_firstname = models.TextField(blank=True)
    contact2_lastname = models.TextField(blank=True)
    contact2_job_title = models.TextField(blank=True)
    contact2_direct_telephone = models.TextField(blank=True)
    contact2_other_telephone = models.TextField(blank=True)
    contact2_mobile_phone_number = models.TextField(blank=True)
    contact2_fax_number = models.TextField(blank=True)
    contact2_unconfirmed_email = models.TextField(blank=True)
    contact2_confirmed_email = models.TextField(blank=True)
    contact2_confirmed_date_time_ip = models.TextField(blank=True)
    customer_address_hbn = models.TextField(blank=True)
    customer_address_street = models.TextField(blank=True)
    customer_address_town = models.TextField(blank=True)
    customer_address_city = models.TextField(blank=True)
    customer_address_county = models.TextField(blank=True)
    customer_address_country = models.TextField(blank=True)
    customer_address_postcode = models.TextField(blank=True)
    billing_address_hbn = models.TextField(blank=True)
    billing_address_street = models.TextField(blank=True)
    billing_address_town = models.TextField(blank=True)
    billing_address_city = models.TextField(blank=True)
    billing_address_county = models.TextField(blank=True)
    billing_address_country = models.TextField(blank=True)
    billing_address_postcode = models.TextField(blank=True)
    general_contact_telephone = models.TextField(blank=True)
    general_contact_fax = models.TextField(blank=True)
    options_customer_group = models.IntegerField(default=0)
    options_credit_terms = models.TextField(blank=True)
    options_credit_limit = models.TextField(blank=True)
    options_current_credit = models.TextField(blank=True)
    class Meta:
        db_table = u'customers'

    def get_customers_by_futurefour_id(self, futurefour_id):
        try:
            return Customers.objects.filter(futurefour_id=futurefour_id)
        except Customers.DoesNotExist:
            return None

class Employee(models.Model):
    employeeid = models.IntegerField(primary_key=True)
    username = models.TextField()
    password = models.TextField(blank=True)
    password_hint = models.TextField(blank=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    employee_number = models.TextField(blank=True)
    futurefour_id = models.CharField(max_length=765, blank=True)
    active_employee = models.IntegerField()
    employee_deleted = models.IntegerField(null=True, blank=True)
    employee_delete_date = models.DateTimeField(null=True, blank=True)
    employee_title = models.IntegerField(null=True, blank=True)
    employee_firstname = models.CharField(max_length=765, blank=True)
    employee_lastname = models.CharField(max_length=765, blank=True)
    employee_job_title = models.CharField(max_length=765, blank=True)
    employee_direct_telephone = models.CharField(max_length=765, blank=True)
    employee_other_telephone = models.CharField(max_length=765, blank=True)
    employee_mobile_phone_number = models.CharField(max_length=765, blank=True)
    employee_fax_number = models.CharField(max_length=765, blank=True)
    employee_unconfirmed_email = models.CharField(max_length=765, blank=True)
    employee_confirmed_email = models.CharField(max_length=765, blank=True)
    employee_confirmed_date_time_ip = models.CharField(max_length=765, blank=True)
    class Meta:
        db_table = u'employee'

class GroupTypes(models.Model):
    typeid = models.IntegerField(primary_key=True)
    type_name = models.TextField(blank=True)
    class Meta:
        db_table = u'group_types'

class PasswordVault(models.Model):
    vaultid = models.IntegerField(primary_key=True)
    vault_userid = models.IntegerField()
    vault_login_location = models.TextField(blank=True)
    vault_username = models.TextField(blank=True)
    vault_password = models.TextField(blank=True)
    class Meta:
        db_table = u'password_vault'

class Products(models.Model):
    productid = models.AutoField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    item_name = models.TextField(blank=True)
    item_number = models.CharField(max_length=765, blank=True)
    description = models.TextField(blank=True)
    categoryid = models.IntegerField(null=True, blank=True)
    items_per_pack = models.IntegerField(null=True, blank=True)
    purch_product_id = models.IntegerField(null=True, blank=True)
    purch_item_name = models.IntegerField(null=True, blank=True)
    purch_item_number = models.IntegerField(null=True, blank=True)
    purch_supp_quant_of = models.TextField(blank=True)
    purch_supplier_cost = models.TextField(blank=True)
    purch_vat_category_id = models.IntegerField(null=True, blank=True)
    purch_supplier_sale_price = models.FloatField(null=True, blank=True)
    assigned_supplier = models.IntegerField(null=True, blank=True)
    suppliers_current_stock = models.IntegerField(null=True, blank=True)
    suppliers_qnty_on_order = models.IntegerField(null=True, blank=True)
    stock_reorder_level = models.IntegerField(null=True, blank=True)
    stock_current_quantity = models.IntegerField(null=True, blank=True)
    stock_quant_on_order = models.IntegerField(null=True, blank=True)
    stock_total_value_of_stock = models.FloatField(null=True, blank=True)
    sales_product_weight = models.TextField(blank=True)
    sales_size_height = models.TextField(blank=True)
    sales_size_width = models.TextField(blank=True)
    sales_size_lenght = models.TextField(blank=True)
    sales_description_on_invoice = models.TextField(blank=True)
    sales_vat_rate = models.IntegerField(null=True, blank=True)
    sales_net_price = models.TextField(blank=True)
    sales_sale_price = models.TextField(blank=True)
    sales_vat_code = models.IntegerField(null=True, blank=True)
    sales_vat_value = models.FloatField(null=True, blank=True)
    stock_on_hand = models.IntegerField(null=True, blank=True)
    incoming_sales = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'products'

    def __unicode__(self):
        return str(self.productid) + '-' + self.item_name

class ProductsAttributes(models.Model):
    attributeid = models.IntegerField(primary_key=True)
    productid = models.IntegerField(null=True, blank=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    attribute_name = models.TextField(blank=True)
    attribute_type = models.IntegerField(null=True, blank=True)
    attribute_order = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'products_attributes'

class ProductsAttributesCategories(models.Model):
    categoryid = models.IntegerField(primary_key=True)
    productid = models.IntegerField(null=True, blank=True)
    attributeid = models.IntegerField(null=True, blank=True)
    dictionaryid = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'products_attributes_categories'

class ProductsAttributesDictionary(models.Model):
    dictionaryid = models.IntegerField(primary_key=True)
    category_name = models.TextField(blank=True)
    category_description = models.TextField(blank=True)
    parentid = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'products_attributes_dictionary'

class ProductsAttributesOptions(models.Model):
    id = models.IntegerField(primary_key=True)
    attributeid = models.IntegerField()
    color = models.CharField(max_length=765)
    class Meta:
        db_table = u'products_attributes_options'

class ProductsAttributesTypes(models.Model):
    typeid = models.IntegerField(primary_key=True)
    type_name = models.TextField(blank=True)
    class Meta:
        db_table = u'products_attributes_types'

class ProductsCategories(models.Model):
    categoryid = models.IntegerField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    category_name = models.TextField()
    group_type_id = models.IntegerField(null=True, blank=True)
    group_category_id = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'products_categories'

class Services(models.Model):
    serviceid = models.IntegerField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    item_name = models.TextField(blank=True)
    item_number = models.TextField(blank=True)
    description = models.TextField(blank=True)
    category = models.IntegerField(null=True, blank=True)
    net_price = models.TextField(blank=True)
    vat_rate = models.FloatField(null=True, blank=True)
    vat_category = models.IntegerField(null=True, blank=True)
    sale_price = models.TextField(blank=True)
    sales_description_on_invoice = models.TextField(blank=True)
    class Meta:
        db_table = u'services'

class ServicesCategories(models.Model):
    categoryid = models.IntegerField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    category_name = models.TextField(blank=True)
    class Meta:
        db_table = u'services_categories'

class StaCode(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=12)
    class Meta:
        db_table = u'sta_code'

class Suppliers(models.Model):
    supplierid = models.IntegerField(primary_key=True)
    supplier_number = models.TextField(blank=True)
    options_supplier_group = models.IntegerField()
    owner_userid = models.IntegerField(null=True, blank=True)
    supplier_deleted = models.IntegerField()
    supplier_delete_date = models.CharField(max_length=765, blank=True)
    active_supplier = models.IntegerField()
    sta_identification_number = models.CharField(max_length=765, blank=True)
    supplier_business_name = models.TextField(blank=True)
    contact1_title = models.IntegerField(null=True, blank=True)
    contact1_firstname = models.TextField(blank=True)
    contact1_lastname = models.TextField(blank=True)
    contact1_job_title = models.TextField(blank=True)
    contact1_direct_telephone = models.TextField(blank=True)
    contact1_other_telephone = models.TextField(blank=True)
    contact1_mobile_telephone = models.TextField(blank=True)
    contact1_fax_number = models.TextField(blank=True)
    contact1_unconfirmed_email = models.TextField(blank=True)
    contact1_confirmed_email = models.TextField(blank=True)
    contact1_confirmed_date_time_ip = models.TextField(blank=True)
    contact2_title = models.IntegerField(null=True, blank=True)
    contact2_firstname = models.TextField(blank=True)
    contact2_lastname = models.TextField(blank=True)
    contact2_job_title = models.TextField(blank=True)
    contact2_direct_telephone = models.TextField(blank=True)
    contact2_other_telephone = models.TextField(blank=True)
    contact2_mobile_telephone = models.TextField(blank=True)
    contact2_fax_number = models.TextField(blank=True)
    contact2_unconfirmed_email = models.TextField(blank=True)
    contact2_confirmed_email = models.TextField(blank=True)
    contact2_confirmed_date_time_ip = models.TextField(blank=True)
    supplier_address_hbn = models.TextField(blank=True)
    supplier_address_street = models.TextField(blank=True)
    supplier_address_town = models.TextField(blank=True)
    supplier_address_city = models.TextField(blank=True)
    supplier_address_county = models.TextField(blank=True)
    supplier_address_country = models.TextField(blank=True)
    supplier_address_postcode = models.TextField(blank=True)
    supplier_address_sta_iman = models.TextField(blank=True)
    billing_address_hbn = models.TextField(blank=True)
    billing_address_street = models.TextField(blank=True)
    billing_address_town = models.TextField(blank=True)
    billing_address_city = models.TextField(blank=True)
    billing_address_county = models.TextField(blank=True)
    billing_address_country = models.TextField(blank=True)
    billing_address_postcode = models.TextField(blank=True)
    company_details_website = models.TextField(blank=True)
    company_details_vat_number = models.TextField(blank=True)
    company_details_customer_id = models.TextField(blank=True)
    company_details_our_creditlimit = models.TextField(blank=True)
    company_details_current_credit = models.TextField(blank=True)
    company_details_current_balance = models.TextField(blank=True)
    company_details_credit_terms = models.TextField(blank=True)
    company_sta_iman = models.TextField(blank=True)
    customer_reference_number = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'suppliers'

class TownCode(models.Model):
    codeid = models.IntegerField(primary_key=True)
    countryid = models.CharField(max_length=30)
    code_name = models.CharField(max_length=765, blank=True)
    available = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'town_code'

class Users(models.Model):
    userid = models.AutoField(primary_key=True)
    username = models.TextField()
    password = models.TextField(blank=True)
    password_hint = models.TextField(blank=True)
    email_address = models.TextField(blank=True)
    firstname = models.TextField(blank=True)
    lastname = models.TextField(blank=True)
    active_user = models.IntegerField(default=1)
    owner_user = models.IntegerField(default=0)
    futurefour_id = models.CharField(max_length=150, blank=True)
    futurefour_username = models.CharField(max_length=765, blank=True)
    sta_identification_number = models.CharField(max_length=150, blank=True)
    private_company_number = models.CharField(max_length=765, blank=True)
    private_address_hbn = models.CharField(max_length=765, blank=True)
    private_business_name = models.TextField(blank=True)
    private_contact_person = models.TextField(blank=True)
    private_street = models.TextField(blank=True)
    private_town = models.TextField(blank=True)
    private_city = models.TextField(blank=True)
    private_county = models.TextField(blank=True)
    private_country = models.TextField(blank=True)
    private_postcode = models.TextField(blank=True)
    private_telephone = models.TextField(blank=True)
    private_fax = models.TextField(blank=True)
    private_unconfirmed_email = models.TextField(blank=True)
    private_confirmed_email = models.TextField(blank=True)
    private_confirmed_date_time_ip = models.TextField(blank=True)
    private_website_address = models.TextField(blank=True)
    private_company_vat_number = models.CharField(max_length=765, blank=True)
    private_sta_iman = models.TextField(blank=True)
    private_selling_default_vat = models.IntegerField(null=True, blank=True)
    pub_ascust_company_businessname = models.TextField(blank=True)
    pub_ascust_company_vat_number = models.CharField(max_length=765, blank=True)
    pub_ascust_company_website = models.TextField(blank=True)
    pub_ascust_company_sta_iman = models.TextField(blank=True)
    pub_ascust_company_comp_number = models.TextField(blank=True)
    pub_ascust_contact1_title = models.IntegerField(null=True, blank=True)
    pub_ascust_contact1_firstname = models.TextField(blank=True)
    pub_ascust_contact1_lastname = models.TextField(blank=True)
    pub_ascust_contact1_job_title = models.TextField(blank=True)
    pub_ascust_contact1_directphone = models.TextField(blank=True)
    pub_ascust_contact1_otherphone = models.TextField(blank=True)
    pub_ascust_contact1_mobilephone = models.TextField(blank=True)
    pub_ascust_contact1_fax = models.TextField(blank=True)
    pub_ascust_contact1_uncon_email = models.TextField(blank=True)
    pub_ascust_contact1_conf_email = models.TextField(blank=True)
    pub_ascust_contact1_conf_d_t_ip = models.TextField(blank=True)
    pub_ascust_contact2_title = models.IntegerField(null=True, blank=True)
    pub_ascust_contact2_firstname = models.TextField(blank=True)
    pub_ascust_contact2_lastname = models.TextField(blank=True)
    pub_ascust_contact2_job_title = models.TextField(blank=True)
    pub_ascust_contact2_directphone = models.TextField(blank=True)
    pub_ascust_contact2_otherphone = models.TextField(blank=True)
    pub_ascust_contact2_mobilephone = models.TextField(blank=True)
    pub_ascust_contact2_fax = models.TextField(blank=True)
    pub_ascust_contact2_uncon_email = models.TextField(blank=True)
    pub_ascust_contact2_conf_email = models.TextField(blank=True)
    pub_ascust_contact2_conf_d_t_ip = models.TextField(blank=True)
    pub_ascust_address_hbn = models.TextField(blank=True)
    pub_ascust_address_street = models.TextField(blank=True)
    pub_ascust_address_town = models.TextField(blank=True)
    pub_ascust_address_city = models.TextField(blank=True)
    pub_ascust_address_county = models.TextField(blank=True)
    pub_ascust_address_country = models.TextField(blank=True)
    pub_ascust_address_postcode = models.TextField(blank=True)
    pub_ascust_billingaddr_hbn = models.TextField(blank=True)
    pub_ascust_billingaddr_street = models.TextField(blank=True)
    pub_ascust_billingaddr_town = models.TextField(blank=True)
    pub_ascust_billingaddr_city = models.TextField(blank=True)
    pub_ascust_billingaddr_county = models.TextField(blank=True)
    pub_ascust_billingaddr_country = models.TextField(blank=True)
    pub_ascust_billingaddr_postcode = models.TextField(blank=True)
    pub_ascust_gral_contact_phone = models.TextField(blank=True)
    pub_ascust_gral_contact_fax = models.TextField(blank=True)
    pub_ascust_selling_default_vat = models.IntegerField(null=True, blank=True)
    pub_assupp_company_businessname = models.TextField(blank=True)
    pub_assupp_company_website = models.TextField(blank=True)
    pub_assupp_company_customerid = models.TextField(blank=True)
    pub_assupp_company_sta_iman = models.TextField(blank=True)
    pub_assupp_selling_default_vat = models.IntegerField(null=True, blank=True)
    pub_assupp_company_vat_number = models.CharField(max_length=765, blank=True)
    pub_assupp_company_comp_number = models.TextField(blank=True)
    pub_assupp_contact1_title = models.IntegerField(null=True, blank=True)
    pub_assupp_contact1_firstname = models.TextField(blank=True)
    pub_assupp_contact1_lastname = models.TextField(blank=True)
    pub_assupp_contact1_job_title = models.TextField(blank=True)
    pub_assupp_contact1_directphone = models.TextField(blank=True)
    pub_assupp_contact1_otherphone = models.TextField(blank=True)
    pub_assupp_contact1_mobilephone = models.TextField(blank=True)
    pub_assupp_contact1_fax = models.TextField(blank=True)
    pub_assupp_contact1_uncon_email = models.TextField(blank=True)
    pub_assupp_contact1_conf_email = models.TextField(blank=True)
    pub_assupp_contact1_conf_d_t_ip = models.TextField(blank=True)
    pub_assupp_contact2_title = models.IntegerField(null=True, blank=True)
    pub_assupp_contact2_firstname = models.TextField(blank=True)
    pub_assupp_contact2_lastname = models.TextField(blank=True)
    pub_assupp_contact2_job_title = models.TextField(blank=True)
    pub_assupp_contact2_directphone = models.TextField(blank=True)
    pub_assupp_contact2_otherphone = models.TextField(blank=True)
    pub_assupp_contact2_mobilephone = models.TextField(blank=True)
    pub_assupp_contact2_fax = models.TextField(blank=True)
    pub_assupp_contact2_uncon_email = models.TextField(blank=True)
    pub_assupp_contact2_conf_email = models.TextField(blank=True)
    pub_assupp_contact2_conf_d_t_ip = models.TextField(blank=True)
    pub_asemp_contact1_title = models.IntegerField(null=True, blank=True)
    pub_asemp_contact1_firstname = models.TextField(blank=True)
    pub_asemp_contact1_lastname = models.TextField(blank=True)
    pub_asemp_contact1_job_title = models.TextField(blank=True)
    pub_asemp_contact1_directphone = models.TextField(blank=True)
    pub_asemp_contact1_otherphone = models.TextField(blank=True)
    pub_asemp_contact1_mobilephone = models.TextField(blank=True)
    pub_asemp_contact1_fax = models.TextField(blank=True)
    pub_asemp_contact1_uncon_email = models.TextField(blank=True)
    pub_asemp_contact1_conf_email = models.TextField(blank=True)
    pub_asemp_contact1_conf_d_t_ip = models.TextField(blank=True)
    pub_asemp_selling_default_vat = models.IntegerField(null=True, blank=True)
    pub_assupp_address_hbn = models.TextField(blank=True)
    pub_assupp_address_street = models.TextField(blank=True)
    pub_assupp_address_town = models.TextField(blank=True)
    pub_assupp_address_city = models.TextField(blank=True)
    pub_assupp_address_county = models.TextField(blank=True)
    pub_assupp_address_country = models.TextField(blank=True)
    pub_assupp_address_postcode = models.TextField(blank=True)
    pub_assupp_billingaddr_hbn = models.TextField(blank=True)
    pub_assupp_billingaddr_street = models.TextField(blank=True)
    pub_assupp_billingaddr_town = models.TextField(blank=True)
    pub_assupp_billingaddr_city = models.TextField(blank=True)
    pub_assupp_billingaddr_county = models.TextField(blank=True)
    pub_assupp_billingaddr_country = models.TextField(blank=True)
    pub_assupp_billingaddr_postcode = models.TextField(blank=True)
    pub_assupp_customer_ref_number = models.TextField(blank=True)
    pub_asaffl_personal_title = models.IntegerField(null=True, blank=True)
    pub_asaffl_personal_firstname = models.TextField(blank=True)
    pub_asaffl_personal_lastname = models.TextField(blank=True)
    pub_asaffl_personal_uncon_email = models.TextField(blank=True)
    pub_asaffl_personal_conf_email = models.TextField(blank=True)
    pub_asaffl_personal_conf_d_t_ip = models.TextField(blank=True)
    pub_asaffl_company_businessname = models.TextField(blank=True)
    pub_asaffl_company_vat_number = models.CharField(max_length=765, blank=True)
    pub_asaffl_company_comp_number = models.TextField(blank=True)
    pub_asaffl_company_sta_iman = models.TextField(blank=True)
    pub_asaffl_selling_default_vat = models.IntegerField(null=True, blank=True)
    pub_asaffl_address_hbn = models.TextField(blank=True)
    pub_asaffl_address_street = models.TextField(blank=True)
    pub_asaffl_address_town = models.TextField(blank=True)
    pub_asaffl_address_city = models.TextField(blank=True)
    pub_asaffl_address_county = models.TextField(blank=True)
    pub_asaffl_address_country = models.TextField(blank=True)
    pub_asaffl_address_postcode = models.TextField(blank=True)
    pub_asaffl_address_pref_payment = models.TextField(blank=True)
    pub_asaffl_address_telephone = models.TextField(blank=True)
    pub_asaffl_mobile_telephone = models.TextField(blank=True)
    pub_asaffl_address_fax = models.TextField(blank=True)
    pub_asaffl_address_website = models.TextField(blank=True)
    title = models.IntegerField(null=True, blank=True)

    user_manager = managers.CurUserManager()

    objects = models.Manager()

    class Meta:
        db_table = u'users'

    def __unicode__(self):
        return str(self.userid) + '-' + self.username

    def check_password(self,password):
        return self.password == password

    def getProfile(self):
        return SfGuardUserProfile.objects.get(cur_user_id=self.userid)

    def is_Supplier(self):
        if(self.sta_identification_number != ''):
            return Suppliers.objects.using('cur').get(sta_identification_number=self.sta_identification_number).count() > 0
        return False

    def is_Customer(self):
        if(self.futurefour_id != ''):
            return Customers.objects.using('cur').get(futurefour_id=self.futurefour_id).count() > 0
        return False

    def get_ssd_user(self):
        try:
            return Users.objects.get(username = 'ssd')
        except Users.DoesNotExist:
            return None

    def get_user_by_email(self, email_address):
        try:
            return Users.objects.get(email_address=email_address)
        except Users.DoesNotExist:
            return None


class UsersPrivileges(models.Model):
    privilegeid = models.IntegerField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    owner_privilege = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'users_privileges'

class UsersTitles(models.Model):
    titleid = models.IntegerField(primary_key=True)
    title = models.TextField(blank=True)
    class Meta:
        db_table = u'users_titles'

class VatGroups(models.Model):
    groupid = models.IntegerField(primary_key=True)
    country_code = models.ForeignKey(CountryCode, null=True, db_column='country_code', blank=True)
    group_name = models.TextField(blank=True)
    group_description = models.TextField(blank=True)
    group_type_id = models.IntegerField(null=True, blank=True)
    group_percentage_rate = models.DecimalField(null=True, max_digits=20, decimal_places=2, blank=True)
    class Meta:
        db_table = u'vat_groups'

class VatSetting(models.Model):
    vatsettingid = models.IntegerField(primary_key=True)
    owner_userid = models.IntegerField(null=True, blank=True)
    provided_vat = models.IntegerField(null=True, blank=True)
    provided_price = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'vat_setting'


site.register(Affiliates)
site.register(ContactGroups)
site.register(CountryCode)
site.register(CreditCardDetails)
site.register(Customers)
site.register(Employee)
site.register(GroupTypes)
site.register(PasswordVault)
site.register(Products)
site.register(ProductsAttributes)
site.register(ProductsAttributesCategories)
site.register(ProductsAttributesDictionary)
site.register(ProductsAttributesOptions)
site.register(ProductsAttributesTypes)
site.register(ProductsCategories)
site.register(Services)
site.register(ServicesCategories)
site.register(StaCode)
site.register(Suppliers)
site.register(TownCode)
site.register(Users)
site.register(UsersPrivileges)
site.register(UsersTitles)
site.register(VatGroups)
site.register(VatSetting)
