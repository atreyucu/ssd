from datetime import datetime
from django.contrib.admin.sites import site
from django.contrib.auth.models import User
from django.db import models
from cur.models import Products
from django.db.models.query_utils import Q


class Message(models.Model):
    user = models.ForeignKey(User)
    subject = models.CharField(max_length=255, blank=True)
    body = models.TextField()
    sent = models.DateTimeField(auto_now=True)

class PublicMessage(models.Model):
    user_email = models.EmailField()
    user_name = models.CharField(max_length=255)
    body = models.TextField()
    date_sent = models.DateTimeField(auto_now=True)

class ProductSize(models.Model):
    name = models.CharField(max_length=255, null=True)
    short_name = models.CharField(max_length=10)

    def __unicode__(self):
        return self.short_name


class SsdProductExtension(models.Model):
    size = models.OneToOneField(ProductSize, unique=True)
    cur_product_id = models.IntegerField(unique=True)
    parent_product_id = models.IntegerField(default=0)

    def getCurProduct(self):
        try:
            return Products.objects.get(productid = self.cur_product_id)
        except Products.DoesNotExist:
            return None

    def __unicode__(self):
        cur_product = self.getCurProduct()
        if cur_product is not None:
            return cur_product.item_name
        else:
            return ""

    def get_all_diferent_product(self):
        try:
            sldp = SsdProductExtension.objects.filter( Q(parent_product_id = None) | Q(parent_product_id = 0))
            return sldp
        except Exception, e:
            return {}

    def get_all_products(self):
        try:
            sldp = SsdProductExtension.objects.all()
            return sldp
        except Exception,e:
            return {}

    def get_sizes(self):
        if self.parent_product_id == 0 or self.parent_product_id == None:
            sldp = SsdProductExtension.objects.filter( Q(id = self.id) | Q(parent_product_id = self.id))
        else:
            sldp = SsdProductExtension.objects.filter( Q(id = self.parent_product_id) | Q(parent_product_id = self.parent_product_id))
        sizes_data = []
        for p in sldp:
            temp = {}
            temp['size_id'] = p.size.id
            temp['size_name'] = p.size.name
            temp['size_short_name'] = p.size.short_name
            temp['sld_product_ext_id'] = p.id
            temp['cur_product_id'] = p.cur_product_id
            sizes_data.append(temp)

        return sizes_data

class PrintedOrders(models.Model):
    order_id = models.IntegerField(default=0)
    printed = models.BooleanField(default = False)
    printed_on = models.DateTimeField(default=datetime.now(), auto_now=False)

    def __unicode__(self):
        return str(self.order_id) + '-' + str(self.printed_on)


class StrengthSequence(models.Model):
    order_number = models.IntegerField(default=1)
    strength_1 = models.ForeignKey(ProductSize, related_name='strength_1_size')
    strength_2 = models.ForeignKey(ProductSize, related_name='strength_2_size')
    strength_3 = models.ForeignKey(ProductSize, related_name='strength_3_size')
    strength_4 = models.ForeignKey(ProductSize, related_name='strength_4_size')
    strength_5 = models.ForeignKey(ProductSize, related_name='strength_5_size')
    strength_6 = models.ForeignKey(ProductSize, related_name='strength_6_size')
    strength_7 = models.ForeignKey(ProductSize, related_name='strength_7_size')
    strength_8 = models.ForeignKey(ProductSize, related_name='strength_8_size')
    strength_9 = models.ForeignKey(ProductSize, related_name='strength_9_size')
    strength_10 = models.ForeignKey(ProductSize, related_name='strength_10_size')



site.register(ProductSize)
site.register(SsdProductExtension)
site.register(StrengthSequence)






