from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response

@permission_required('general_auth.can_access_ouradmins')
def new_orders_today(request):
    return render_to_response('ouradmins/home/orders_today.html')

@permission_required('general_auth.can_access_ouradmins')
def total_subscribers(request):
    return render_to_response('ouradmins/home/total_subscribers.html')

@permission_required('general_auth.can_access_ouradmins')
def printed_orders(request):
    return render_to_response('ouradmins/home/printed_orders.html')

@permission_required('general_auth.can_access_ouradmins')
def orders(request):
    return render_to_response('ouradmins/home/orders.html')

@permission_required('general_auth.can_access_ouradmins')
def new_orders(request):
    return render_to_response('ouradmins/home/new_orders.html')


def current_subscribers(request):
    return render_to_response('ouradmins/subscribers/current_subscribers.html')

def past_subscribers(request):
    return render_to_response('ouradmins/subscribers/past_subscribers.html')


def package_details(request):
    return render_to_response('ouradmins/package_details/package_details.html')

def messages(request):
    return render_to_response('ouradmins/messages/messages.html')