from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'admins.views.new_orders_today'),
   (r'^total_subscribers/$', 'admins.views.total_subscribers'),
   (r'^orders/$', 'admins.views.orders'),
   (r'^new_orders/$', 'admins.views.new_orders'),
   (r'^printed_orders/$', 'admins.views.printed_orders'),


   (r'^packagesdetails/$', 'admins.views.package_details'),


   (r'^subscribers/$', 'admins.views.current_subscribers'),
   (r'^past_subscribers/$', 'admins.views.past_subscribers'),

   (r'^messages/$', 'admins.views.messages'),
)
