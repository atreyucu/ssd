# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."
        from django.core.management import call_command
        call_command("loaddata", "../admin_initial_data.json")

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'admins.message': {
            'Meta': {'object_name': 'Message'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'admins.printedorders': {
            'Meta': {'object_name': 'PrintedOrders'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'printed_on': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 28, 0, 0)'})
        },
        u'admins.productsize': {
            'Meta': {'object_name': 'ProductSize'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'admins.publicmessage': {
            'Meta': {'object_name': 'PublicMessage'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'user_name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'admins.ssdproductextension': {
            'Meta': {'object_name': 'SsdProductExtension'},
            'cur_product_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_product_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'size': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['admins.ProductSize']", 'unique': 'True'})
        },
        u'admins.strengthsequence': {
            'Meta': {'object_name': 'StrengthSequence'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_number': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'strength_1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_1_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_10': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_10_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_2_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_3': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_3_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_4': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_4_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_5': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_5_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_6': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_6_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_7': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_7_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_8': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_8_size'", 'to': u"orm['admins.ProductSize']"}),
            'strength_9': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'strength_9_size'", 'to': u"orm['admins.ProductSize']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['admins']
    symmetrical = True
