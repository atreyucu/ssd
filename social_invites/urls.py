from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^invite_proxy/(?P<invite_id>\d+)/$', 'social_invites.views.invite_proxy'),
   (r'^facebook_popup$', 'social_invites.views.facebook_popup'),
   (r'^twitter_popup$', 'social_invites.views.twitter_popup'),
   (r'^email_popup$', 'social_invites.views.email_popup'),
)
