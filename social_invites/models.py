from django.db import models
from django.contrib.auth.models import User

# Create your models here.
CHN_OPTIONS=(('fb','Facebook'),('tw','Twitter'),('em','Email'))

class InviteRegister(models.Model):
    user = models.ForeignKey(User)
    channel = models.CharField(max_length=2,choices=CHN_OPTIONS)
    send_date = models.DateTimeField(auto_now=True)
    reply_date = models.DateTimeField(auto_now=False)
    is_follow = models.BooleanField(default=False)
    is_sign_up = models.BooleanField(default=False)