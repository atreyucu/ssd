# -*- coding: utf-8 -*-
import simplejson
from django.shortcuts import render_to_response
import ssd.settings as settings
from django.http import HttpResponseRedirect, HttpResponse
from models import InviteRegister
import datetime
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from general_auth.models import GeneralUser
from django.contrib.auth.decorators import permission_required

@permission_required('general_auth.can_access_useradmins')
def facebook_popup(request):
    if(request.is_ajax()):
        fb_app_id = settings.INV_FB_APP_ID
        fb_app_dialog_img = settings.INV_FB_DIALOG_IMG
        invite = InviteRegister()
        invite.send_date = datetime.datetime.now()
        invite.user = request.user
        invite.reply_date = datetime.datetime.now()
        invite.is_follow = False
        invite.channel = 'fb'
        invite.save()
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name)
        else:
            host_name = 'http://%s'%(host_name)
        db_user = User.objects.get(id=request.user.id)
        g_user = GeneralUser.objects.get(user = db_user)
        cigatteres_cost_saving = 365*settings.COST_OF_CIGARRETES*g_user.cigarette_smoked_per_day
        return render_to_response('social_invites/sl_fb_popup.html', {'fb_app_id': fb_app_id, 'fb_invite_id':str(invite.id),'fb_app_dialog_img': fb_app_dialog_img, 'cigatteres_cost_saving': cigatteres_cost_saving, 'host_name': host_name})
    else:
        return HttpResponse()

@csrf_exempt
@permission_required('general_auth.can_access_useradmins')
def email_popup(request):
    if(request.is_ajax()):
        db_user = User.objects.get(id=request.user.id)
        g_user = GeneralUser.objects.get(user = db_user)
        cigatteres_cost_saving = 365*settings.COST_OF_CIGARRETES*g_user.cigarette_smoked_per_day
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name) + '/invite_proxy/'
        else:
            host_name = 'http://%s'%(host_name) + '/invite_proxy/'
        return render_to_response('social_invites/sl_email_popup.html',{'cigatteres_cost_saving': cigatteres_cost_saving, 'link':host_name })
    if(request.method == 'POST'):
            to_value = request.POST['to'].split(',')
            subject_value = request.POST['subject']
            message_value = request.POST['message']
            try:
                for to in to_value:
                     invite = InviteRegister()
                     invite.send_date = datetime.datetime.now()
                     invite.user = request.user
                     invite.reply_date = datetime.datetime.now()
                     invite.is_follow = False
                     invite.channel = 'em'
                     invite.save()

                     send_mail(subject_value, message_value + str(invite.id), settings.SLD_FROM_USER,to_value)
            except Exception:
                return HttpResponseRedirect('/usersadmin/')
            return HttpResponseRedirect('/usersadmin/')
    else:
        return HttpResponse()

@permission_required('general_auth.can_access_useradmins')
def twitter_popup(request):
    if(request.is_ajax()):
        db_user = User.objects.get(id=request.user.id)
        g_user = GeneralUser.objects.get(user = db_user)
        cigatteres_cost_saving = 365*settings.COST_OF_CIGARRETES*g_user.cigarette_smoked_per_day
        invite = InviteRegister()
        invite.send_date = datetime.datetime.now()
        invite.user = request.user
        invite.reply_date = datetime.datetime.now()
        invite.is_follow = False
        invite.channel = 'tw'
        invite.save()
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name)
        else:
            host_name = 'http://%s'%(host_name)
        result_dict = {'saving_cigarretes': cigatteres_cost_saving, 'host_name': host_name, 'inv_id': invite.id}
        json = simplejson.dumps(result_dict)
        return HttpResponse(json, content_type="application/json")
    else:
        return HttpResponse()


def invite_proxy(request,invite_id):
    if(InviteRegister.objects.filter(id=invite_id)):
        invite = InviteRegister.objects.get(id=invite_id)
        invite.is_follow = True
        invite.reply_date = datetime.datetime.now()
        invite.save()

        request.session['ssd_customer_follow_invite'] = invite_id

        return HttpResponseRedirect('/signup/')
    else:
        return HttpResponseRedirect('/')
