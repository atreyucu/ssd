/**
 * Created by ernesto on 4/12/13.
 */

/*
 * Time Zone
 * Cuba -4
 * UK +1
 *
 **/
var global_timezone = '+11';
$(document).ready(function(){
        if( $("#end_date").length > 0)
        {
            var date_input = $("#end_date").val();
            var date_arr = date_input.split(':');
            var year = date_arr[0];
            var month = date_arr[1];
            var month_temp = parseInt(month) -1;
            var day = date_arr[2];
            var hour = parseInt(date_arr[3]) + 1;
            var minutes = date_arr[4];
            var second = date_arr[5]
            var newDate = new Date(year, month_temp, day, hour, minutes, second);

            $('#signup_countdown').countdown({
                until: newDate,
                format: 'MS',
                layout: '<span>({mn} minute{sep} </span><span>{sn} seconds left)</span> {desc}',
                timezone: -5

            });
        }
    $("#form_input_flavour").submit(function(){
        return validate_flavour_form()
    })

    $("#form_email_password").submit(function(){
        return validate_email_password_form();
    })
    $("#good_news_form").submit(function(){
        return validate_good_news_form();
    })
    $("#form_input_address").submit(function(){
        return validate_form_input_address();
    })

    $('label[for=id_captcha_1]').remove();
    $('div.captcha_form > input').each(function(){
       $(this).addClass('input_contact_us');
       $(this).attr('size', '25')
    });
    $('div.captcha_form > img').each(function(){
       $(this).attr('width', '180px').attr('height','30px')
    });

    $('#public_contact_submit').click(function(){
        var valid = validate_contact_us();
        var email = email_validation($('#email'))
        if (valid && email)
            $('#public_contact_form').submit();
    });

    $('#start_saving').click(function(){
       $(location).attr('href','/save94');
    });
    $('div.fragrance_left_row > input#start_saving').click(function(){
       $(location).attr('href','/signup');
    });

    $('#subscribe_right').click(function(){
       $(location).attr('href','/signup');
    });
    $('#subscribe_left').click(function(){
       $(location).attr('href','/signup');
    });

    $('#password_submit').click(function(){
       var email = email_validation($('#email'))
       if (email)
        $('#form_passwor_recover').submit();
    });
})

function get_products(select, index)
{
    var category_selected = $(select);
    var product_select = $("#product_id_"+index);
    product_select.html("<option value='0'> -- Select product --</option>")
    $.get('/product_category/'+category_selected.val(),
        function(data){
            for( p in data)
            {
                var option = $("<option>");
                option.attr("value", data[p].sld_product_ext_id);
                option.html(data[p].name);
                product_select.append(option);

            }


        })
}

function get_sizes_of_product(select_product, select_size)
{

    var product_selected = $(select_product);
    var size_select = $("#"+select_size);
    size_select.html("<option value='0'> Select size </option>")
    $.get('/sizes_product/'+product_selected.val(),
        function(data){
            for( p in data)
            {
                var option = $("<option>");
                option.attr("value", data[p].sld_product_ext_id);
                option.html(data[p].size_name);
                size_select.append(option);

            }


        })
}

function validate_flavour_form()
{
    var valid = true;
    $("select").each(function(index, elem)
            {
                var jq_elem = $(elem);
                if( jq_elem.val() == 0)
                {
                    valid = false;
                }
            });
    return valid;
}

function validate_email_password_form()
{
    var valid = true;
    valid = find_empty_input()

    var email_address = $("#email_address");
    if (email_validation(email_address) == false)
    {
       valid = false;
    }
    var remail_address = $("#remail_address")
    if (remail_address.val() != email_address.val())
    {
        valid = false
        remail_address.css('border-color','#ff0000');

    }
    var password = $("#password")
    var rpassword = $("#rpassword")
    if (password.val() != rpassword.val())
    {
        valid = false;
        rpassword.css('border-color','#ff0000');
    }
    return valid;
}

function validate_good_news_form(){
    var valid = true;
    valid = find_empty_input();
    var email_address = $("#email_address");
    if (email_validation(email_address) == false)
    {
       valid = false;
       email_address.css('border-color','#ff0000');
    }
    return valid
}


function validate_form_input_address()
{
    var valid = true;
    valid = find_empty_input();
    return valid

}

function find_empty_input()
{
    var valid = true;
        $("input[type=text]").each(function(index, elem)
            {
                var jq_elem = $(elem);
                if( jq_elem.val() == 0)
                {
                    valid = false;
                    jq_elem.css('border-color','#ff0000');
                }
                else{
                jq_elem.css('border-color','#999999');
                }
            });
    $("input[type=password]").each(function(index, elem)
        {
            var jq_elem = $(elem);
            if( jq_elem.val() == 0)
            {
                valid = false;
                jq_elem.css('border-color','#ff0000');
            }
            else{
                jq_elem.css('border-color','#999999');
            }
        });
    $("select").each(function(index, elem)
        {
            var jq_elem = $(elem);
            if( jq_elem.val() == 0)
            {
                valid = false;
                jq_elem.addClass('input_error');
            }
            else{
                jq_elem.removeClass('input_error');
            }
        })
    return valid;
}

function email_validation(email_value){
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
     if(filter.test(email_value.val()))
     {
         email_value.removeClass('input_error');
         return true;
     }
    else
    {
        email_value.addClass('input_error');
        return false;
    }
}

function validate_contact_us(){
    var valid = true;
    $("input[type=text]").each(function(index, elem)
            {
                var jq_elem = $(elem);
                if( jq_elem.val() == 0)
                {
                    jq_elem.css('border-color','#ff0000');
                    valid = false;
                }
                else{
                jq_elem.css('border-color','#999999');
                }
            });
    return valid;
}