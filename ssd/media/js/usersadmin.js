/**
 * Created by ramdy on 22/01/14.
 */

$(document).ready(function(){
    $('#submit_contactus').click(submit_contact_us);
    $('#submit_yourdetails').click(submit_your_details);
    $.get('/socialinvites/twitter_popup',function(data){
        var url = "http://twitter.com/share?";
        var  text = "Switch from cigarettes to smoking liquid and save £100’s, I have and I will be saving £" + data['saving_cigarretes'] + "every year!! check it out here..";
        var real_url = data['host_name'] + '/socialinvites/invite_proxy/' + data['inv_id']
        var final_url = url + "text=" + text + "&url=" + real_url;
        $("#twitter_popup").attr("href", final_url);
        $('#twitter_popup').click(function(event) {
        var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                     ',width='  + width  +
                     ',height=' + height +
                     ',top='    + top    +
                     ',left='   + left;

        window.open(final_url, 'Twitter Invite', opts);
        return false;
    });
  });
  $('#em').jqm({ajax: '/socialinvites/email_popup', trigger: 'a.emtrigger'});
  $('#fb').jqm({ajax: '/socialinvites/facebook_popup', trigger: 'a.fbtrigger'});
});

function submit_contact_us(){
    var subject = $('#subject').val();
    var body = $('#body').val();
    if (body != "" || subject != ""){
        $("#contactus").submit();
    }
}

function submit_your_details(){
    $("#submit_details").submit();
}

function get_towns(town, country){
    var option = "<option selected='selected' value=''>Choose your town/city</option>";
    $.get("/usersadmin/get_towns",
        {"country": country.val()},
        function(data){
            for (value in data){
                var html = "<option value='"+value+"'>"+ data[value] + " </option>";
                option += html;
            }
            town.html(option);
        },
        'JSON')
}

function change_flavour(number)
{
    var flavour_selected = $("#flavour_"+number).val();
    $.get('/usersadmin/change_flavour',{'number': number, 'flavour': flavour_selected},
                function(data){
                    console.log('okok')
                }
            , 'json')

}
function change_status_order()
{
    $.get('/usersadmin/change_status_order', {},
            function(data)
            {
                if(data.success)
                {
                    $("#change_delivery").html(data.text)
                }
            }
    ,'json')
}