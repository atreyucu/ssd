from django.conf.urls import patterns, include, url
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'prd.views.home', name='home'),
    # url(r'^prd/', include('prd.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^ouradmin/', include('admins.urls')),
    url(r'^usersadmin/', include('users.urls')),
    url(r'^socialinvites/', include('social_invites.urls')),
    url(r'^', include('public.urls')),
    url(r'^admin/', include(admin.site.urls)),
    (r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'general_auth/login.html'}),
    (r'^accounts/logout/$', 'general_auth.views.logout_view'),
    url(r'^captcha/', include('captcha.urls')),


)

