import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'media/templates'),
)

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

DATABASE_ROUTERS = ['general_auth.routers.AuthRouter']

AUTHENTICATION_BACKENDS=('general_auth.backends.GeneralAuthBackend','django.contrib.auth.backends.ModelBackend')

CAPTCHA_FONT_SIZE = 28

CAPTCHA_LETTER_ROTATION = (-20,20)

CAPTCHA_NOISE_FUNCTIONS = ""

LOGIN_REDIRECT_URL = '/usersadmin'

#Configuration of the invites
INV_FB_APP_ID = '552873124805448'

INV_FB_DIALOG_IMG = 'http://localhost:8000/media/images/public/logo.png'

COST_OF_CIGARRETES = 0.35

EMAIL_HOST = '192.168.50.1'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'ernesto.lazo@vcl.desoft.cu'
EMAIL_HOST_PASSWORD = 'pass123'
SSD_FROM_USER = 'ssd@smokeliguid.com'
#defaul value
EMAIL_USE_TLS = False


#standing information constants
#weeks counts
DEFAULT_REBILLING_PERIOD = 4
#DAYS COUNTS
DEFAULT_DELIVERY_PERIOD = 56

#Payment module selection
DEFAULT_ACTIVE_PAYMENT_MODULE = 'SAGE'


#Sage Integration
SAGEPAY_URL = 'https://test.sagepay.com/gateway/service/vspserver-register.vsp'
SAGE_VENDOR_NAME = 'seller1seller'
SAGE_DEFAULT_TXTYPE = 'PAYMENT'
SAGE_VPS_PROTOCOL = '3.00'
SAGE_CURRENCY = 'GBP'

#Trasactions data
SSD_MONTH_ORDER_AMMOUNT = 19.99

#Payment repeat manager vars
PAYMENT_REPEAT_REBILLING_PERIOD = 1

PAYMENT_REPEAT_DELIVERY_PERIOD = 56

ALLOWED_HOSTS = ['*']