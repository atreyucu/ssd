from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'public.views.home'),
   (r'^ourpackages/$', 'public.views.our_packages'),
   (r'^productdetails/$', 'public.views.product_details'),
   (r'^faq/$', 'public.views.faqs'),
   (r'^save94/$', 'public.views.save94'),
   (r'^flavours/$', 'public.views.flavours'),
   (r'^signin/$', 'public.views.sign_in'),
   (r'^contact_us/$', 'public.views.contact_us'),
   (r'^login/$', 'public.views.login'),

   (r'^signup/$', 'public.views.sign_up'),
   (r'^signup/middle_step/$', 'public.views.middle_step'),
   (r'^signup/good_news/$', 'public.views.good_news'),
   (r'^signup/good_news2/$', 'public.views.good_news2'),


   #(r'^signup/input_flavours$', 'public.views.sign_up_step2'),
   (r'^signup/input_address$', 'public.views.sign_up_step2'),

   (r'^signup/save_delivery_data$', 'public.views.get_delivery_data'),


   #payment url
   (r'^payment_successful$', 'public.views.payment_successful'),
   (r'^payment_error$', 'public.views.payment_error'),

   (r'^aboutus/$', 'public.views.about_us'),
   (r'^signup/$', 'public.views.sign_up'),
   (r'^password_recover/$', 'public.views.forgot_passwd'),
   (r'^sizes_product/(?P<sldproduct_id>.*)$', 'public.views.get_sizes_of_product'),
   (r'^payment_proxy/$', 'public.views.payment_proxy'),
   (r'^sage_notification_url/$', 'public.views.sage_notification_url'),
   (r'^sage_complete_url/(?P<value>\d+)/$', 'public.views.sage_complete_url'),

)