import datetime
from django.contrib import messages
from django.contrib.auth.models import User, Permission
from django.core.mail import send_mail
from django.http import HttpResponse, Http404
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from admins.models import SsdProductExtension, PublicMessage, PrintedOrders, StrengthSequence, ProductSize
import json
from cor.models import Orderr, Item
from cur.models import Users, Customers, UsersTitles, CountryCode, TownCode, Products
from django.db import transaction
from general_auth.models import GeneralUser, Flavours
from public.forms import CaptchaForm
from social_invites.models import InviteRegister
from users.models import StandingOrderInformation
import ssd.settings as settings
import uuid
from datetime import timedelta
from django_sagepay.utils import encode_transaction_request, decode_transaction_response
import requests
from requests.exceptions import RequestException


def home(request):
    return render_to_response('public/index.html',context_instance = RequestContext(request))

def our_packages(request):
    return HttpResponse("Our packages page under construction")

def product_details(request):
    return HttpResponse("Product details page under construction")

def faqs(request):
    return render_to_response('public/faq.html',context_instance = RequestContext(request))

def save94(request):
    return render_to_response('public/save94.html',context_instance = RequestContext(request))

def flavours(request):
    return render_to_response('public/flavours.html',context_instance = RequestContext(request))

def contact_us(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/contactus/')
    if request.POST:
        form = CaptchaForm(request.POST)
        name = request.POST['name']
        email = request.POST['email']
        message = request.POST['message']
        if form.is_valid():
            public_message = PublicMessage(user_name=name, user_email=email, body=message)
            public_message.save()
            return render_to_response('public/contact_us.html', {'form': form, 'captcha': 1},context_instance = RequestContext(request))
        else:
            return render_to_response('public/contact_us.html', {'form': form, 'captcha': 0, 'name':name, 'email':email, 'message':message},context_instance = RequestContext(request))
    else:
        form = CaptchaForm()
        return render_to_response('public/contact_us.html', {'form': form, 'captcha': -1},context_instance = RequestContext(request))

def login(request):
    return render_to_response('public/login.html', context_instance= RequestContext(request))

def about_us(request):
    return HttpResponse("About page under construction")

def sign_in(request):
    return HttpResponse("Signin page under construction")

def sign_up(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    from datetime import timedelta, date, time
    date_today = datetime.datetime.now()
    date_today_5m_plus =date_today + timedelta(minutes=5)
    request.session['signup_end_date'] ='%s:%s:%s:%s:%s:%s'%(date_today_5m_plus.year, date_today_5m_plus.month, date_today_5m_plus.day,date_today_5m_plus.hour,date_today_5m_plus.minute, str(date_today_5m_plus.second))
    request.session['comming'] = 'sign_up'
    return render_to_response('public/signup_step1.html', context_instance= RequestContext(request))


def middle_step(request):
    #---------obteniendo los datos--------------------------
    if request.POST.has_key('email_address') and  request.POST.has_key('remail_address') and request.POST.has_key('password') and request.POST.has_key('rpassword'):
        email_address = request.POST.get('email_address')
        remail_address = request.POST.get('remail_address')
        password = request.POST.get('password')
        rpassword = request.POST.get('rpassword')
        if password != rpassword or email_address != remail_address:
            return HttpResponseRedirect('/signup')
        if email_address == "":
            return HttpResponseRedirect('/signup')
        request.session['user_data'] = {'email_address': email_address, 'password': password}
        cur_users = Users().get_user_by_email(email_address)
        if cur_users is None:
            request.session['comming'] = 'good_news'
            return HttpResponseRedirect('/signup/input_address?pass=p')

        customer_users = Customers().get_customers_by_futurefour_id(cur_users.futurefour_id)
        ssd_user = Users().get_ssd_user()
        message_subject = 'SSD credentials'
        message_body = 'username: %s /n password: %s'%(cur_users.username, cur_users.password)

        try:
            send_mail(message_subject,message_body,settings.SSD_FROM_USER,cur_users.username)
        except Exception, e:
            pass

        for cu in customer_users:
            if cu.owner_userid == ssd_user.userid:
                return HttpResponseRedirect('/signup/good_news')
        return HttpResponseRedirect('/signup/good_news2')
    else:
        raise Http404

def good_news(request):
    if True or request.session.has_key('comming') and request.session['comming'] == 'sign_up':
        if request.GET.has_key('error') and request.GET.get('error') == 'p':
            message_error = "Your username and password didn't match. Please try again."
        else:
            message_error = ''
        request.session['comming'] = 'good_news'
        return render_to_response('public/good_news.html', {'message_error': message_error}, context_instance= RequestContext(request))
    else:
        return HttpResponseRedirect('/signup/')


def good_news2(request):
    if True or request.session.has_key('comming') and request.session['comming'] == 'sign_up':
        if request.GET.has_key('error') and request.GET.get('error') == 'p':
            message_error = "Your username and password didn't match. Please try again."
        else:
            message_error = ''
        request.session['comming'] = 'good_news'
        return render_to_response('public/good_news2.html', {'message_error': message_error}, context_instance= RequestContext(request))
    else:
        return HttpResponseRedirect('/signup/')

def sign_up_step2(request):
    email = ''
    password = ''
    if True or request.session.has_key('comming') and request.session['comming'] == 'good_news':
        if request.GET.has_key('pass') and request.GET.get('pass') == 'p':
            pass
        else:
            #comprobar que el usuario y el password esten bien
            if request.POST.has_key('email') and request.POST.has_key('password'):
                email = request.POST.get('email')
                password = request.POST.get('password')
                try:
                    cur_user = Users.objects.get(email_address=email)
                    if cur_user.password != password:
                        messages.error(request, 'The user or password are wrong.')
                        request.session['comming'] = 'sign_up'
                        return HttpResponseRedirect(request.META['HTTP_REFERER']+'?error=p')
                except Users.DoesNotExist:
                    request.session['comming'] = 'sign_up'
                    return HttpResponseRedirect(request.META['HTTP_REFERER']+'?error=p')
            else:
                raise Http404

        try:
            django_user = User.objects.get(username = email)
            general_user = GeneralUser.objects.get(user = django_user)
        except User.DoesNotExist:
            django_user = None
            general_user = None
        except GeneralUser.DoesNotExist:
            general_user = None
        except Exception, e:
            return render_to_response('500.html')


        #obtener datos a devolver
        titles = UsersTitles.objects.all()
        temp_titles = {}
        for t in titles:
            temp_titles[t.titleid] = t.title

        countrys = CountryCode.objects.all()
        temp_country = {}
        for c in countrys:
            temp_country[c.codeid] = c.code_name

        towns_code = TownCode.objects.all()
        temp_town = {}
        for tc in towns_code:
            temp_town[tc.codeid] = tc.code_name

        user_email = request.session['user_data']['email_address']
        try:
            django_user = User.objects.get(username = user_email)
            general_user = GeneralUser.objects.get(user = django_user)
        except User.DoesNotExist:
            django_user = None
            general_user = None
        except GeneralUser.DoesNotExist:
            django_user = None
            general_user = None
        except Exception, e:
            return render_to_response('500.html')
        if django_user is not None and general_user is not None:
            user_data = {'title': general_user.your_title, 'first_name': general_user.your_firstname, 'last_name': general_user.your_secondname,
                        'hbn': general_user.your_hbn, 'street_address': general_user.your_streetaddress, 'street_2_line': general_user.your_secondlineaddress,
                        'town': general_user.your_towncity, 'country': general_user.your_country, 'postcode': general_user.your_postcode}
        else:
            user_data = {'title': '', 'first_name': '', 'last_name': '',
                        'hbn': '', 'street_address': '', 'street_2_line': '',
                        'town': 0, 'country': 0, 'postcode': ''}

        request.session['comming'] = 'input_address'
        return render_to_response('public/input_address.html', {'user_data': user_data, 'titles': temp_titles, 'country': temp_country, 'town': temp_town}, context_instance= RequestContext(request))
    else:
        return HttpResponseRedirect('/signup/')
#
#def sign_up_step3(request):
#    if True or request.session.has_key('comming') and request.session['comming'] == 'input_flavours':
#        if request.POST.has_key("flavour_1") and request.POST.has_key("flavour_2") and request.POST.has_key("flavour_3")   and request.POST.has_key("flavour_4") and request.POST.has_key("flavour_5"):
#            #obteniendo los datos del paso  2
#            sld_product_id_1 = request.POST["flavour_1"]
#            sld_product_id_2 = request.POST["flavour_2"]
#            sld_product_id_3 = request.POST["flavour_3"]
#            sld_product_id_4 = request.POST["flavour_4"]
#            sld_product_id_5 = request.POST["flavour_5"]
#
#
#            request.session['sld_products'] = {}
#            request.session['sld_products']['sld_product_id_1'] = sld_product_id_1
#            request.session['sld_products']['sld_product_id_2'] = sld_product_id_2
#            request.session['sld_products']['sld_product_id_3'] = sld_product_id_3
#            request.session['sld_products']['sld_product_id_4'] = sld_product_id_4
#            request.session['sld_products']['sld_product_id_5'] = sld_product_id_5
#
#            request.session['cigarrettes_per_day'] = request.POST['cigarrettes_per_day']
#
#
#            #obtener datos a devolver
#            titles = UsersTitles.objects.all()
#            temp_titles = {}
#            for t in titles:
#                temp_titles[t.titleid] = t.title
#
#            countrys = CountryCode.objects.all()
#            temp_country = {}
#            for c in countrys:
#                temp_country[c.codeid] = c.code_name
#
#            towns_code = TownCode.objects.all()
#            temp_town = {}
#            for tc in towns_code:
#                temp_town[tc.codeid] = tc.code_name
#
#            user_email = request.session['user_data']['email_address']
#            try:
#                django_user = User.objects.get(username = user_email)
#                general_user = GeneralUser.objects.get(user = django_user)
#            except User.DoesNotExist:
#                django_user = None
#                general_user = None
#            except GeneralUser.DoesNotExist:
#                django_user = None
#                general_user = None
#            except Exception, e:
#                return render_to_response('500.html')
#            if django_user is not None and general_user is not None:
#                user_data = {'title': general_user.your_title, 'first_name': general_user.your_firstname, 'last_name': general_user.your_secondname,
#                            'hbn': general_user.your_hbn, 'street_address': general_user.your_streetaddress, 'street_2_line': general_user.your_secondlineaddress,
#                            'town': general_user.your_towncity, 'country': general_user.your_country, 'postcode': general_user.your_postcode}
#            else:
#                user_data = {'title': '', 'first_name': '', 'last_name': '',
#                            'hbn': '', 'street_address': '', 'street_2_line': '',
#                            'town': 0, 'country': 0, 'postcode': ''}
#
#            request.session['comming'] = 'input_address'
#            return render_to_response('public/input_address.html', {'user_data': user_data, 'titles': temp_titles, 'country': temp_country, 'town': temp_town}, context_instance= RequestContext(request))
#        else:
#            raise Http404
#    else:
#        return HttpResponseRedirect('/signup/')

def payment_successful(request):
    return render_to_response('public/payment_successful.html',context_instance = RequestContext(request))

def payment_error(request):
    return render_to_response('public/payment_error.html',context_instance = RequestContext(request))

@transaction.commit_manually
def get_delivery_data(request):
    if True or request.session.has_key('comming') and request.session['comming'] == 'input_address':
        if request.POST.has_key('title') and request.POST.has_key('firstname') and request.POST.has_key('lastname') and request.POST.has_key('hbn') and request.POST.has_key('street_address') and request.POST.has_key('street_address_2line') and request.POST.has_key('towncity') and request.POST.has_key('country') and request.POST.has_key('postcode'):
            #obteniendo datos del input_address
            title = request.POST.get('title')
            firstname = request.POST.get('firstname')
            lastname = request.POST.get('lastname')
            hbn = request.POST.get('hbn')
            street_address= request.POST.get('street_address')
            street_address_2line = request.POST.get('street_address_2line')
            towncity = request.POST.get('towncity')
            country = request.POST.get('country')
            postcode = request.POST.get('postcode')

            #guardando en la session
            temp =  request.session['user_data']
            request.session['delivery_data'] = {}
            request.session['delivery_data']['title'] = title
            request.session['delivery_data']['firstname'] = firstname
            request.session['delivery_data']['lastname'] = lastname
            request.session['delivery_data']['hbn'] = hbn
            request.session['delivery_data']['street_address'] = street_address
            request.session['delivery_data']['towncity'] = towncity
            request.session['delivery_data']['street_address_2line'] = street_address_2line
            request.session['delivery_data']['country'] = country
            request.session['delivery_data']['postcode'] = postcode
            temp1 = request.session['user_data']
            try:
                user_data = request.session['user_data']
                cur_user = Users().get_user_by_email(user_data['email_address'])

                if cur_user is None:
                    #esto se ejecuta en caso que el correo no exista en la tabla USERs
                    cur_user = Users()
                    cur_user.username = user_data['email_address']
                    cur_user.firstname = firstname
                    cur_user.lastname = lastname
                    cur_user.email_address = user_data['email_address']
                    cur_user.password = user_data['password']
                    cur_user.private_address_hbn = hbn
                    cur_user.private_city =  street_address_2line
                    cur_user.private_town = towncity
                    cur_user.private_street = street_address
                    cur_user.private_country = country
                    cur_user.private_postcode = postcode

                    cur_user.pub_ascust_contact1_firstname = firstname
                    cur_user.pub_ascust_contact1_lastname = lastname
                    cur_user.pub_ascust_contact1_title = title

                    cur_user.pub_ascust_address_city = street_address_2line
                    cur_user.pub_ascust_address_country = country
                    cur_user.pub_ascust_address_hbn = hbn
                    cur_user.pub_ascust_address_postcode = postcode
                    cur_user.pub_ascust_address_street = street_address
                    cur_user.pub_ascust_address_town = towncity

                    cur_user.pub_ascust_contact2_firstname = firstname
                    cur_user.pub_ascust_contact2_lastname = lastname
                    cur_user.pub_ascust_contact2_title = title

                    cur_user.pub_ascust_billingaddr_city = street_address_2line
                    cur_user.pub_ascust_billingaddr_country = country
                    cur_user.pub_ascust_billingaddr_hbn = hbn
                    cur_user.pub_ascust_billingaddr_postcode = postcode
                    cur_user.pub_ascust_billingaddr_street = street_address
                    cur_user.pub_ascust_billingaddr_town = towncity

                    cur_user.save()

                cur_user = None
                cur_user = Users().get_user_by_email(user_data['email_address'])
                customer_users_arr = Customers().get_customers_by_futurefour_id(cur_user.futurefour_id)
                ssd_user = Users().get_ssd_user()
                if ssd_user is None:
                    transaction.rollback()
                    return render_to_response('500.html')
                    return HttpResponse('Error ssd user does not exists.')
                exists_customer = False

                for cu in customer_users_arr:
                        if cu.owner_userid == ssd_user.userid:
                            customer_users = cu
                            exists_customer = True
                            break
                if not exists_customer:
                    #aqui hay que guardar el usuario como customer
                    customer_users = Customers()
                    customer_users.futurefour_id = cur_user.futurefour_id
                    customer_users.owner_userid = ssd_user.userid
                    customer_users.contact1_title = title
                    customer_users.contact1_firstname = firstname
                    customer_users.contact1_lastname = lastname
                    customer_users.contact1_confirmed_email = user_data['email_address']

                    customer_users.contact2_title = title
                    customer_users.contact2_firstname = firstname
                    customer_users.contact2_lastname = lastname
                    customer_users.contact2_confirmed_email = user_data['email_address']

                    customer_users.customer_address_hbn = hbn
                    customer_users.customer_address_street = street_address
                    customer_users.customer_address_town = towncity
                    customer_users.customer_address_city = street_address_2line
                    customer_users.customer_address_country = country
                    customer_users.customer_address_postcode = postcode

                    customer_users.billing_address_hbn = hbn
                    customer_users.billing_address_street = street_address
                    customer_users.billing_address_town = towncity
                    customer_users.billing_address_city = street_address_2line
                    customer_users.billing_address_country = country
                    customer_users.billing_address_postcode = postcode

                    customer_users.save()


                general_user = GeneralUser().get_guser_by_email(user_data['email_address'])
                if general_user is None:
                    user_django = User()
                    general_user = GeneralUser()
                else:
                    user_django = general_user.user

                user_django.username = user_data['email_address']
                user_django.set_password(user_data['password'])
                user_django.email = user_data['email_address']
                user_django.first_name = firstname
                user_django.last_name = lastname
                permission = Permission.objects.get(codename = 'can_access_useradmins')

                user_django.save()
                user_django.user_permissions.add(permission)
                user_django.save()

                general_user.user = user_django
                general_user.cur_user_id = cur_user.userid
                general_user.email = user_data['email_address']
                general_user.password = user_data['password']
                general_user.your_title = title
                general_user.your_firstname = firstname
                general_user.your_secondname = lastname
                general_user.your_hbn = hbn
                general_user.your_streetaddress = street_address
                general_user.your_secondlineaddress = street_address_2line
                general_user.your_towncity = towncity
                general_user.your_country = country
                general_user.your_postcode = postcode

                general_user.save()

                mentol_product_arr = SsdProductExtension.objects.all()
                if len(mentol_product_arr) > 0:
                    mentol_product = mentol_product_arr[0]
                else:
                    transaction.rollback()
                    return render_to_response('500.html')



                strength_sequence_arr = StrengthSequence.objects.filter(order_number = 1)
                if len(strength_sequence_arr) > 0:
                    strength_sequence = strength_sequence_arr[0]
                else:
                    return render_to_response('500.html')


                product_cur = mentol_product.getCurProduct()
                #creando la order en la tabla
                soi  = StandingOrderInformation()
                soi.owner_user_id = product_cur.owner_userid
                soi.user_id = cur_user.userid
                soi.order_date = datetime.datetime.now()
                soi.last_success_rebilling_date = datetime.datetime.now()
                #soi.unsubscribe_recived = None
                #soi.unsubscribe_approved = None
                soi.rebilling_period = settings.DEFAULT_REBILLING_PERIOD
                soi.next_rebilling = soi.last_success_rebilling_date + timedelta(weeks=soi.rebilling_period)
                soi.delivery_period = settings.DEFAULT_DELIVERY_PERIOD
                soi.last_delivery_printed = datetime.datetime.now()
                soi.next_delivery_printed = soi.last_delivery_printed + timedelta(days=46)
                soi.sequence_strength_id = strength_sequence.id
                soi.save()

                general_user.standing_order_information = soi
                general_user.save()

                # OJO aqui generar una orden para esta trasaccion
                order = Orderr()
                #customer_users = Customers()
                order.customer_id = cur_user.userid
                order.customer_idn_number = 0
                order.customer_futurefour_id = customer_users.futurefour_id
                order.customer_number = customer_users.customerid
                order.customer_business_name = customer_users.contact1_firstname + ' ' + customer_users.contact1_lastname
                order.customer_phone = customer_users.contact1_mobile_phone_number
                order.customer_mobile = customer_users.contact1_mobile_phone_number
                order.customer_email = customer_users.contact1_confirmed_email
                order.customer_fax = customer_users.contact1_fax_number

                order.application_signature = 'SSD'
                order.delivery_cost = 0
                order.delivery_vat = 0
                order.order_total = settings.SSD_MONTH_ORDER_AMMOUNT
                order.order_subtotal = settings.SSD_MONTH_ORDER_AMMOUNT

                order.deliver_order_to_address_hbn = customer_users.customer_address_hbn
                order.deliver_to_address_street = customer_users.customer_address_street
                order.deliver_to_address_town = customer_users.customer_address_town
                order.deliver_to_address_city = customer_users.customer_address_city
                order.deliver_to_address_country = customer_users.customer_address_country
                order.deliver_to_address_county = customer_users.customer_address_county
                order.deliver_to_address_postcode = customer_users.customer_address_postcode

                order.billing_order_to_address_hbn = customer_users.billing_address_hbn
                order.billing_to_address_street = customer_users.billing_address_street
                order.billing_to_address_town = customer_users.billing_address_town
                order.billing_to_address_city = customer_users.billing_address_city
                order.billing_to_address_country = customer_users.billing_address_country
                order.billing_to_address_county = customer_users.billing_address_county
                order.billing_to_address_postcode = customer_users.billing_address_postcode

                import time
                order.order_generation_date = time.time()
                order.save()

                #item para la order
                for i in range(1,11):
                    if i == 1:
                        ssd_product_size = strength_sequence.strength_1
                    elif i == 2:
                        ssd_product_size = strength_sequence.strength_2
                    elif i == 3:
                        ssd_product_size = strength_sequence.strength_3
                    elif i == 4:
                        ssd_product_size = strength_sequence.strength_4
                    elif i == 5:
                        ssd_product_size = strength_sequence.strength_5
                    elif i == 6:
                        ssd_product_size = strength_sequence.strength_6
                    elif i == 7:
                        ssd_product_size = strength_sequence.strength_7
                    elif i == 8:
                        ssd_product_size = strength_sequence.strength_8
                    elif i == 9:
                        ssd_product_size = strength_sequence.strength_9
                    elif i == 10:
                        ssd_product_size = strength_sequence.strength_10

                    ssd_product = ssd_product_size.ssdproductextension
                    cur_product = ssd_product.getCurProduct()
                    if cur_product is None:
                        transaction.rollback()
                        return render_to_response('500.html')

                    item = Item()
                    item.tran = order
                    item.supplier_item_name = cur_product.item_name
                    item.supplier_item_number = cur_product.item_number
                    item.supplier_product_id = cur_product.productid
                    #ver si hay que cambiar esto
                    item.supplier_net = 0
                    item.supplier_gross = 0
                    item.supplier_vat_rate = 0
                    item.supplier_vat_value = 0
                    item.save()


                printed_order = PrintedOrders()
                printed_order.order_id = order.tran_id
                printed_order.save()

                soi.last_delivery_batch_id = printed_order.id
                soi.save()


                if request.session.has_key('ssd_customer_follow_invite'):
                    inv_id = request.session['ssd_customer_follow_invite']
                    invite = InviteRegister.objects.get(id=inv_id)
                    invite.is_sign_up = True
                    invite.save()
                    db_user = User.objects.get(id=request.user.id)
                    soi = db_user.generaluser.standing_order_information
                    soi.number_of_free_pipe += 1
                    soi.free_vaporizer = True
                    soi.save()
                    request.session.delete('ssd_customer_follow_invite')
                from django.contrib.auth import authenticate, login
                user_authenticate = authenticate(username =  user_django.username, password =  user_data['password'])
                login(request, user_authenticate)
                transaction.commit()
                request.session['comming'] = 'save_input_address'
                return HttpResponseRedirect('/payment_proxy/')
            except Exception, e:
                transaction.rollback()
                return render_to_response('500.html')
                return HttpResponse(e)
        else:
            raise Http404
    else:
        return HttpResponseRedirect('/signup/')


def forgot_passwd(request):
    try:
        if request.POST:
            email = request.POST['email']
            user = User.objects.get(username=email)
            cur_user = Users.objects.get(pk=user.generaluser.cur_user_id)

            send_mail("Password Recover", cur_user.password, settings.SSD_FROM_USER, [email])

            return render_to_response("public/password_recover.html", {"sucefull":True}, context_instance=RequestContext(request))
        else:
             return render_to_response("public/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))
    except User.DoesNotExist, e:
        return render_to_response("public/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))
    except Exception, e:
        return render_to_response("public/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))

#vistas llamdas por ajax
def get_sizes_of_product(request, ssd_product_id):
    sizes = []
    if ssd_product_id != '0' and ssd_product_id != 0:
        try:
            ssdproduct = SsdProductExtension.objects.get(id = ssd_product_id)
            sizes = ssdproduct.get_sizes()
            data = json.dumps(sizes)
        except SsdProductExtension.DoesNotExist:
            return HttpResponse("no hay producto")
    else:
        data = json.dumps({})
    return HttpResponse(data, content_type='application/json')

def payment_proxy(request):
    if request.session.has_key('comming') and request.session['comming'] == 'save_input_address':
        if settings.DEFAULT_ACTIVE_PAYMENT_MODULE == 'SAGE':
            notification_url = ""
            if request.is_secure():
                notification_url = 'https://'
            else:
                notification_url = 'http://'

            notification_url = notification_url + request.META['HTTP_HOST'] + '/sage_notification_url/'

            batch_id = str(uuid.uuid4())

            data = {
                    'VPSProtocol': settings.SAGE_VPS_PROTOCOL,
                    'TxType': settings.SAGE_DEFAULT_TXTYPE,
                    'VendorTxCode': batch_id,  # Generate a new transaction ID
                    'Vendor': settings.SAGE_VENDOR_NAME,
                    'Amount': settings.SSD_MONTH_ORDER_AMMOUNT,
                    'Currency': settings.SAGE_CURRENCY,
                    'Description': 'Smoking liquid order',
                    'BillingSurname':  request.session['delivery_data']['lastname'][0:19],  # 20 Chars
                    'BillingFirstnames': request.session['delivery_data']['firstname'][0:19], # 20 Chars
                    'BillingAddress1': request.session['delivery_data']['street_address'][0:99],  # Truncate to 100 Chars
                    'BillingCity': request.session['delivery_data']['towncity'][0:39],  # Truncate to 40 Chars
                    'BillingPostCode': request.session['delivery_data']['postcode'][0:9],  # Truncate to 10 Chars
                    'BillingCountry': 'GB',#request.session['delivery_data']['country'][0:1],  # 2 letter country code
                    'DeliverySurname': request.session['delivery_data']['lastname'][0:19],  # 20 Chars
                    'DeliveryFirstnames':request.session['delivery_data']['firstname'][0:19],  # 20 Chars
                    'DeliveryAddress1':  request.session['delivery_data']['street_address'][0:99],  # 100 Chars
                    'DeliveryCity': request.session['delivery_data']['towncity'][0:39], # 40 Chars
                    'DeliveryPostCode': request.session['delivery_data']['postcode'][0:9],  # 10 Chars
                    'DeliveryCountry': 'GB',#request.session['delivery_data']['country'][0:1],  # 2 letter country code
                    'NotificationURL':notification_url,
                    'CreateToken': 1
                }

            request_body = encode_transaction_request(data)

            try:
                # Fire request to SagePay which creates the transaction
                response = requests.post(settings.SAGEPAY_URL, data=request_body,
                                         headers={"Content-Type": "application/x-www-form-urlencoded"})
                # Does nothing on 200, but raises exceptions for other statuses
                response.raise_for_status()
                # RequestException covers network/DNS related problems as well as non-200
                # responses
            except RequestException as e:
                user_data = request.session['user_data']

                user = User.objects.get(username=user_data['email_address'])
                soi = user.generaluser.standing_order_information
                soi.rebilling_id = ''
                soi.VPSProtocol = data['VPSProtocol']
                soi.TxType = data['TxType']
                soi.VendorTxCode = data['VendorTxCode']
                soi.Vendor = data['Vendor']
                soi.Amount = data['Amount']
                soi.Currency = data['Currency']
                soi.Description = data['Description']
                soi.BillingSurname = data['BillingSurname']
                soi.BillingFirstnames = data['BillingFirstnames']
                soi.BillingAddress1 = data['BillingAddress1']
                soi.BillingCity = data['BillingCity']
                soi.BillingPostCode = data['BillingPostCode']
                soi.BillingCountry = data['BillingCountry']
                soi.DeliverySurname = data['DeliverySurname']
                soi.DeliveryFirstnames = data['DeliveryFirstnames']
                soi.DeliveryAddress1 = data['DeliveryAddress1']
                soi.DeliveryCity = data['DeliveryCity']
                soi.DeliveryPostCode = data['DeliveryPostCode']
                soi.DeliveryCountry = data['DeliveryCountry']
                soi.NotificationURL = data['NotificationURL']
                soi.CreateToken = 1
                soi.save()
                return render_to_response('public/payment_error.html',{'payment_error_detail':e.message},context_instance = RequestContext(request))

            response_data = decode_transaction_response(response.text)

            if response_data['Status'] == 'OK':
                try:
                    user_data = request.session['user_data']

                    user = User.objects.get(username=user_data['email_address'])
                    soi = user.generaluser.standing_order_information
                    soi.rebilling_id = response_data['VPSTxId']
                    soi.VPSProtocol = data['VPSProtocol']
                    soi.TxType = data['TxType']
                    soi.VendorTxCode = data['VendorTxCode']
                    soi.Vendor = data['Vendor']
                    soi.Amount = data['Amount']
                    soi.Currency = data['Currency']
                    soi.Description = data['Description']
                    soi.BillingSurname = data['BillingSurname']
                    soi.BillingFirstnames = data['BillingFirstnames']
                    soi.BillingAddress1 = data['BillingAddress1']
                    soi.BillingCity = data['BillingCity']
                    soi.BillingPostCode = data['BillingPostCode']
                    soi.BillingCountry = data['BillingCountry']
                    soi.DeliverySurname = data['DeliverySurname']
                    soi.DeliveryFirstnames = data['DeliveryFirstnames']
                    soi.DeliveryAddress1 = data['DeliveryAddress1']
                    soi.DeliveryCity = data['DeliveryCity']
                    soi.DeliveryPostCode = data['DeliveryPostCode']
                    soi.DeliveryCountry = data['DeliveryCountry']
                    soi.NotificationURL = data['NotificationURL']
                    soi.CreateToken = 1
                    soi.save()
                    return HttpResponseRedirect(response_data['NextURL'])
                except Exception, ex:
                    return render_to_response('public/payment_error.html',{'payment_error_detail': "Error !. Saving the Sage Transaction. %s"%(ex.message)},context_instance = RequestContext(request))
            else:
                user_data = request.session['user_data']

                user = User.objects.get(username=user_data['email_address'])
                soi = user.generaluser.standing_order_information
                soi.rebilling_id = ''
                soi.VPSProtocol = data['VPSProtocol']
                soi.TxType = data['TxType']
                soi.VendorTxCode = data['VendorTxCode']
                soi.Vendor = data['Vendor']
                soi.Amount = data['Amount']
                soi.Currency = data['Currency']
                soi.Description = data['Description']
                soi.BillingSurname = data['BillingSurname']
                soi.BillingFirstnames = data['BillingFirstnames']
                soi.BillingAddress1 = data['BillingAddress1']
                soi.BillingCity = data['BillingCity']
                soi.BillingPostCode = data['BillingPostCode']
                soi.BillingCountry = data['BillingCountry']
                soi.DeliverySurname = data['DeliverySurname']
                soi.DeliveryFirstnames = data['DeliveryFirstnames']
                soi.DeliveryAddress1 = data['DeliveryAddress1']
                soi.DeliveryCity = data['DeliveryCity']
                soi.DeliveryPostCode = data['DeliveryPostCode']
                soi.DeliveryCountry = data['DeliveryCountry']
                soi.NotificationURL = data['NotificationURL']
                soi.CreateToken = 1
                soi.save()
                return render_to_response('public/payment_error.html', {'payment_error_detail': response_data['StatusDetail']},
                                      context_instance=RequestContext(request))
        elif settings.DEFAULT_ACTIVE_PAYMENT_MODULE == 'PAYPAL':
            pass
    else:
        return HttpResponseRedirect('/signup/')


def sage_notification_url(request):
    if request.method == 'POST':
        try:
            redirect_url = ""
            if request.is_secure():
                redirect_url = 'https://'
            else:
                redirect_url = 'http://'

            redirect_url = redirect_url + request.META['HTTP_HOST'] + '/sage_complete_url/'
            if(request.POST['Status'] == 'OK'):
                data = {
                    'Status': 'OK',
                    'RedirectURL': redirect_url + '1',
                    'StatusDetail': 'Transaction ok',
                    'sage_url': settings.SAGEPAY_URL,
                }
            else:
                data = {
                    'Status': 'OK',
                    'RedirectURL': redirect_url + '0',
                    'StatusDetail': 'The transaction did not complete',
                    'sage_url': settings.SAGEPAY_URL,
                }

            return render_to_response('public/payment_complete',data,context_instance = RequestContext(request))
        except:
            return render_to_response('public/payment_error.html',{'payment_error_detail':'Unexpected Error !! Redirecting from Sage'},context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('/signup/')


def sage_complete_url(request,value):
    pass


#class a(object):
#    def test(self):
#        size = ProductSize.objects.all()
#        parent_id = 0
#        for s in size:
#            cur_product = Products()
#            cur_product.owner_userid = 40
#            cur_product.item_name = 'mentol'
#            cur_product.item_number = 'mentol number'
#            cur_product.description = 'mentol description'
#            cur_product.save()
#            ssd_product = SsdProductExtension()
#            ssd_product.size = s
#            ssd_product.cur_product_id = cur_product.productid
#            ssd_product.parent_product_id = parent_id
#            ssd_product.save()
#            if parent_id == 0:
#                parent_id = ssd_product.id
#            print size
