__author__ = 'ernesto'

from django.template.base import Library
register = Library()


@register.inclusion_tag('public/custom_tag_free_delivery.html', takes_context = True)
def get_div_free_delivery(context):
    request = context['request']
    if request.session.has_key('signup_end_date'):
        date =  request.session['signup_end_date']

    return {'date': date}
