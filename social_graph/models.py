from __future__ import unicode_literals

from django.db import models
from django.contrib.admin import site

class SfGuardUserProfile(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    cur_user_id = models.IntegerField()
    employee_user_id = models.IntegerField()
    first_signin = models.IntegerField(null=True, blank=True)
    level_id = models.IntegerField(null=True, blank=True)
    first_name = models.CharField(max_length=20L)
    last_name = models.CharField(max_length=30L)
    birthday = models.DateTimeField()
    sex = models.IntegerField()
    totalcoins = models.FloatField()
    last_position = models.IntegerField()
    actual_position = models.IntegerField()
    email = models.CharField(max_length=30L)
    photo = models.CharField(max_length=100L)
    city = models.CharField(max_length=50L)
    abaoutme = models.CharField(max_length=100L)
    view_birthday = models.IntegerField(null=True, blank=True)
    view_profile = models.IntegerField(null=True, blank=True)
    first_order = models.IntegerField(null=True, blank=True)
    friend_requests = models.IntegerField()
    last_level_changed_date = models.DateTimeField()
    order_confirmation = models.TextField(blank=True)
    order_confirmation_delay = models.TextField(blank=True)
    product_delivery = models.TextField(blank=True)
    refund_confirmation = models.TextField(blank=True)
    product_return_information = models.TextField(blank=True)
    product_delivery_information = models.TextField(blank=True)
    first_visited_product = models.IntegerField(null=True, blank=True)
    api_key = models.CharField(max_length=255L, blank=True)
    store_url = models.TextField(blank=True)
    trading_name = models.CharField(max_length=255L, blank=True)
    logo_picture = models.CharField(max_length=255L, blank=True)
    signup_date = models.DateTimeField()
    accepted_challenge = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'sf_guard_user_profile'

site.register(SfGuardUserProfile)